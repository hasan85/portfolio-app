FROM node:carbon

# Create app directory
ADD . /code
# Set app directory
WORKDIR /code

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production

# Bundle app source
COPY . .

EXPOSE 7575
CMD [ "npm", "run", "start-prod" ]
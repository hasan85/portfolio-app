const user = (state = {}, action) => {
  switch (action.type) {
    case 'REG_USER':
      return {...action.user}
    case "LOGOUT_USER":
      return {}
    default:
      return state
  }
}

export default user
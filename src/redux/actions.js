export const registerUser = user => ({
    type: "REG_USER",
    user
})

export const loaderIgnitor = isLoading => ({
    type: "LOADING",
    isLoading
})

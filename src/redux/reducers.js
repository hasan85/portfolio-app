import {combineReducers} from 'redux'
import user from './reducers/user'
import loader from './reducers/loader'

const reducers = {
  user,
  loader,
}

export default combineReducers(reducers)
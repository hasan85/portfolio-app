import React, { Component } from 'react';
import * as $ from 'jquery'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'

class ScrollBar extends Component {

    componentDidMount() {
        this._customScrollBar()
    }

    _customScrollBar = () => {
        if (this.ref) {
            const scrollBar = $(this.ref);
            scrollBar.mCustomScrollbar({
                theme: "light-2",
                autoHideScrollbar: true,
                mouseWheel: {
                    enable: true,
                    scrollAmount: 170,
                    normalizeDelta: true
                }
            })
        }
    }

    render() {
        return (
            <div ref={div => (this.ref = div)} className={`scroll-content ${this.props.className}`}>
                {this.props.children}
            </div>
        )
    }
}

export default ScrollBar;
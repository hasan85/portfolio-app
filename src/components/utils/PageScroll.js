import React, { Component } from 'react';
import * as $ from 'jquery'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min'

class PageScroll extends Component {

    componentDidMount() {
        this._customScrollBar()
    }

    _customScrollBar = () => {
        if (this.ref) {
            const scrollBar = $(this.ref);
            scrollBar.mCustomScrollbar({
                theme: "dark",
                mouseWheel: {
                    enable:true,
                    scrollAmount: 270,
                    normalizeDelta: true
                }
            })

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                scrollBar.mCustomScrollbar("destroy");
            }
        }
    }

    render() {
        return (
            <div ref={div => (this.ref = div)} className={`pagescroll ${this.props.className}`}>
                {this.props.children}
            </div>
        )
    }
}

export default PageScroll;
import 'fullpage.js'
import * as $ from 'jquery'
import React, {Component} from 'react';

class FullPage extends Component {

	componentDidMount() {
		this._slideFullPage()
	}

	componentWillUnmount() {
		this._destroyFullPage()
	}

	_slideFullPage = () => {
		if (this.ref && this.ref.length > 0) {
			const slide = $(this.ref);
			slide.fullpage({
				navigation: false, scrollBar: false
			});
		}
	}

	_destroyFullPage = () => {
		if (this.ref && this.ref.length > 0) {
			const slide = $(this.ref);
			slide.fullpage.destroy('all');
		}
	}

	render() {
		return (
			<div ref={div => (this.ref = div)} id="fullpage">
				{this.props.children}
			</div>
		)
	}
}

export default FullPage;

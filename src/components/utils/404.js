import React from 'react';

export const NoMatch = props => {
    return (
        <div className="oopss">
            <div id="error-text">
                <span>404</span>
                {/* <code>{props.location.pathname}</code> */}
                <p>PAGE NOT FOUND</p>
                <p className="hmpg">
                    <button className="back" onClick={() => props.history.goBack()} >BACK</button>
                </p>
            </div>
        </div>
    )
}
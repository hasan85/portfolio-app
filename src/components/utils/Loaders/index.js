import React from 'react';
import './style/loader_dots.css';
import './style/loader_squares.css';
import './style/loader_grid.css';
import './style/loader_bounce_.css';
import './style/loader_blink_dot.css';
import './style/loader_cube_spinner.css';
import './style/loader_cube_slider.css';
import './style/spinner-bouncer.css';
import './style/ripple.css';

export const LoaderDots = () => (
  <div className="spinner">
    <div className="dot1" />
    <div className="dot2" />
  </div>
);

export const LoaderSquares = () => (
  <div className="sk-folding-cube">
    <div className="sk-cube1 sk-cube" />
    <div className="sk-cube2 sk-cube" />
    <div className="sk-cube4 sk-cube" />
    <div className="sk-cube3 sk-cube" />
  </div>
);

export const LoaderGrid = () => (
  <div className="sk-cube-grid">
    <div className="sk-cube sk-cube1" />
    <div className="sk-cube sk-cube2" />
    <div className="sk-cube sk-cube3" />
    <div className="sk-cube sk-cube4" />
    <div className="sk-cube sk-cube5" />
    <div className="sk-cube sk-cube6" />
    <div className="sk-cube sk-cube7" />
    <div className="sk-cube sk-cube8" />
    <div className="sk-cube sk-cube9" />
  </div>
);

export const LoaderBounce_ = () => (
  <div className="spinner-bounce">
    <div className="bounce1" />
    <div className="bounce2" />
    <div className="bounce3" />
  </div>
);

export const LoaderBlinkDot = () => <div className="spinner-blink" />;

export const LoaderCubeSpinner = () => (
  <div className="spinner-cube">
    <div className="cube1" />
    <div className="cube2" />
  </div>
);

export const LoaderCubeSlider = () => <div className="slider" />;

export const LoaderCodeColors = () => (
  <div className="spinner-bouncer">
    <div className="double-bounce1" />
    <div className="double-bounce2" />
  </div>
);

export const RippleLoader = () => <div className="lds-ripple"><div></div><div></div></div>
import firebase from 'firebase'
import {css} from 'glamor'
import _ from 'lodash'
import React, {Component} from 'react'
import {Modal} from 'react-bootstrap'
import {withRouter} from 'react-router-dom'
import {toast, ToastContainer} from 'react-toastify'
import {LoaderDots as Loader} from '../../../../utils/Loaders'
import {emailPattern} from "../../../../utils/pattern"
import './blog-modal.css'

const initialState = {
	isLoading: false,
	blog: {},
	open: true,
	name: '',
	email: '',
	comment: '',
	slug: '',
	errors: {},
	comments: [],
};

class BlogModal extends Component {
	state = {
		...initialState,
	};

	getBlog = slug => {
		firebase
			.database()
			.ref(`/blog/`)
			.orderByChild('slug')
			.equalTo(slug)
			.once('value')
			.then(ss => {
				const _blog = ss.val();
				if (_blog) {
					const blog = _blog[Object.keys(_blog)[0]];
					this.setState({blog})
				} else {
					this.setState({blog: null})
				}
				this.getComments();
			})
	};

	getComments = () => {
		const {blog} = this.state;
		firebase
			.database()
			.ref('/comments/')
			.orderByChild('post_id')
			.equalTo(blog.id)
			.on('value', ss => {
				const _comments = ss.val();
				const comments = [];
				console.log(blog.id);
				if (_comments) {
					Object.keys(_comments).forEach(key => {
						comments.push(_comments[key])
					});
					comments.reverse()
				}
				this.setState({comments}, () => console.log('comment->', comments))
			})
	};

	formatDate = date => {
		var monthNames = [
			"Jan", "Feb", "Mar",
			"Apr", "May", "Jun",
			"Jul", "Aug", "Sep",
			"Oct", "Nov", "Dec",
		];
		const _date = new Date(date);
		const day = _date.getDate();
		const monthIndex = _date.getMonth();
		const year = _date.getFullYear();
		return day + ' ' + monthNames[monthIndex] + ' ' + year
	};

	componentDidMount() {
		const {pathname} = this.props.location;
		const path_arr = pathname.split('/'); // ["", "blog" , "blog_slug", ""]
		const path_arr_filtered = path_arr.filter(
			x => x !== (undefined || null || '')
		);
		const slug = path_arr_filtered.reverse()[0];
		this.setState({slug});
		this.getBlog(slug)
	}

	handleClose = () => {
		this.setState({open: !this.state.open}, () => {
			setTimeout(() => {
				this.props.history.push('/blog')
			}, 200)
		})
	};

	notify = message => {
		toast(message, {
			position: toast.POSITION.BOTTOM_RIGHT,
			className: css({
				background: '#263238',
				color: '#04A8F4',
			}),
		})
	};

	isValid = () => {
		const {name, email, comment} = this.state;
		const errors = {};
		if (_.isEmpty(name)) {
			errors.name = 'This field is required'
		}
		if (_.isEmpty(email)) {
			errors.email = 'This field is required'
		}
		if (_.isEmpty(comment)) {
			errors.comment = 'This field is required'
		}
		if (!emailPattern.test(email)) {
			errors.email = 'Email is not valid'
		}
		this.setState({errors});
		return _.isEmpty(errors)
	};

	reset_state = () => {
		this.setState({...initialState})
	};

	handleSubmit = e => {
		e && e.preventDefault();
		const {name, email, comment, blog} = this.state;
		if (this.isValid()) {
			const date = new Date();
			const blog_comment = {
				name,
				email,
				comment,
				date: date.toString(),
				post_id: blog.id,
			};
			firebase.database().ref('/comments/').push(blog_comment).then(() => {
				this.notify(`you said ${comment}`);
				this.setState({
					name: '',
					email: '',
					comment: '',
				})
			})
		}
	};

	handleChange = e =>
		this.setState({
			[e.target.name]: e.target.value,
		});

	render() {
		const {isLoading, name, email, comment, blog, errors, comments} = this.state;
		return (
			<Modal
				className="blog-modal"
				show={this.state.open}
				onHide={this.handleClose}
			>
				<div className="modal-header">
          <span className="close" onClick={this.handleClose}>
            <i className="pe-7s-close" aria-hidden="true"/>
          </span>
				</div>
				{_.isEmpty(blog)
					? <Loader/>
					: <div>
						<Modal.Body>
							<div
								className="intro-header"
								style={{
									background: `url(${blog.image.url}) center center no-repeat`,
									backgroundSize: 'cover',
								}}
							>
								<div className="blog-header-layer"/>
								<div className="container">
									<div className="row">
										<div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
											{blog
												? <div className="post-heading">
													<div className="posts-tag">
														{blog.tags && blog.tags.map((tag, i) => (
															<span key={i} className="label ">{tag}</span>
														))}
													</div>
													<h1>{blog.title}</h1>
													<span className="meta">
                                <strong className="authorlink">
                                  {blog.author}
                                </strong>
														{blog.date}
                              </span>
												</div>
												: <p> There is no post </p>}
										</div>
									</div>
								</div>
							</div>

							<article>
								<div className="container">
									<div className="row">
										<div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
											{blog ? blog.content : <p/>}
										</div>
									</div>
								</div>
							</article>

							<div className="container">
								<div className="row">
									<div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
										<ol className="comment-list">
											{comments.map((comment, i) => (
												<li key={i}>
													<div className="comment">
														<div className="comment-header">
															<p className="comment-date">
																On {this.formatDate(comment.date)}
															</p>
															<h3 className="comment-title">
																<strong>{comment.name}</strong> commented:
															</h3>
														</div>
														<div className="comment-body">
															<p>
																{comment.comment}
															</p>
														</div>
													</div>
												</li>
											))}
										</ol>

										<div className="comment-form">
											<h4>Leave a comment</h4>

											<fieldset id="comment_form">
												<form>
													<div id="result"/>
													<input
														name="name"
														placeholder="NAME"
														type="text"
														value={name}
														onChange={this.handleChange}
													/>
													<b>{errors.name}</b>
													<input
														name="email"
														placeholder="EMAIL"
														type="email"
														value={email}
														onChange={this.handleChange}
													/>
													<b>{errors.email}</b>
													<textarea
														name="comment"
														placeholder="COMMENT"
														value={comment}
														onChange={this.handleChange}
													/>
													<div><b>{errors.message}</b></div>
													<button
														disabled={isLoading}
														onClick={this.handleSubmit}
														className="btn-default btn btn-sm"
													>
														POST COMMENT
													</button>
												</form>
											</fieldset>
										</div>
									</div>
								</div>
							</div>
						</Modal.Body>
					</div>}
				<ToastContainer/>
			</Modal>
		)
	}
}

export default withRouter(BlogModal)

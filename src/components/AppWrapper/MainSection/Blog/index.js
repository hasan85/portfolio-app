import 'animate.css';
import firebase from 'firebase';
import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {ModalContainer, ModalRoute} from 'react-router-modal';
import 'react-router-modal/css/react-router-modal.css';
import ReactSafeHtml from 'react-safe-html';
import WOW from 'wow.js/dist/wow.js';
import {LoaderDots as Loader} from '../../../utils/Loaders';
import PageScroll from '../../../utils/PageScroll';
import './blog.css';
// import blog1 from './blog1.jpg'
import BlogModal from './BlogModal';

class Blog extends Component {
	state = {
		blogObjects: [],
		sectionClass: 'fadeIn',
		blogModalShow: true,
		loading: true,
		openedBlog: {},
	};

	componentDidMount() {
		this._wowAnimation();
		this.setState({
			sectionClass: 'fadeIn',
		});
		this._getCards();
	}

	componentWillReceiveProps(nextProps) {
		console.log('nextProps-> ', nextProps);
	}

	_wowAnimation = () => {
		if (this.ref) {
			new WOW().init();
		}
	};

	_blogModalToggle = index => {
		this.setState({
			blogModalShow: !this.state.blogModalShow,
		});
	};

	_getComments = () => {
		const {blogObjects} = this.state;
		if (blogObjects) {
			blogObjects.forEach(blog => {
				if (blog.id) {
					let comment_count = 0;
					firebase
						.database()
						.ref('comments')
						.orderByChild('post_id')
						.equalTo(blog.id)
						.on('value', ss => {
							const comments = ss.val();
							if (comments) {
								comment_count = Object.keys(comments).length;
							} else {
								comment_count = 0;
							}
							blog.comment_count = comment_count;
							this.setState({blogObjects});
						});
				}
			});
		}
	};

	_getCards = () => {
		this.setState({loading: true});
		firebase.database().ref('blog').orderByKey().on('value', ss => {
			const data = ss.val();
			const _data = [];
			if (data) {
				Object.keys(data).forEach(key => {
					_data.push(data[key]);
				});
				_data.reverse();
				this.setState({
					blogObjects: _data,
					loading: false,
				});
			} else {
				this.setState({
					blogObjects: _data,
					loading: false,
				});
			}
			this._getComments();
		});
	};

	render() {
		const {openedBlog, loading, blogModalShow, blogObjects} = this.state;
		return (
			<PageScroll
				className={`blog page-current page wow ${this.state.sectionClass} animated`}
			>
				<div className="featured-background">
					<div className="backgroundimg">
						<div className="headerdes-container">
							<fieldset>
								<legend>MY NEWS AND BLOG</legend>
								<i className="peicon pe-7s-news-paper" aria-hidden="true"/>
								<h3>BLOG</h3>
							</fieldset>
						</div>
					</div>
				</div>
				<div className="thecontainer">
					{/* ROUTE for modal  */}
					<ModalRoute
						path={`${this.props.match.url}/:slug`}
						component={BlogModal}
						props={{
							open: blogModalShow,
							close: this._blogModalToggle,
							modalObject: {...openedBlog},
						}}
					/>
					{/* Container for modal */}
					<ModalContainer/>
					<div className="row">
						<div className="col-md-12">
							<div className="title-container">
								<div className="titlebg">
									<div className="icon-title">
										<i className="fa fa-newspaper-o" aria-hidden="true"/>
									</div>
									<div className="smalltitle">Last News</div>
								</div>
							</div>
						</div>
						{blogObjects && !loading
							? blogObjects.map((blog, index) => (
								<div
									key={index}
									className="blogcol col-sm-6 col-xs-12 col-md-4"
								>
									<div className="blog-card-main">
										<div
											className="card-wrapper"
											style={{
												background: `url(${blog && blog.image && blog.image.url}) center/cover no-repeat`,
											}}
										>
											<div className="card-wrapper-layer"/>
											<div className="card-header">
												<div className="date">
													<span className="postday">{blog.date}</span>
												</div>
												<ul className="menu-content">
													<li>
														<a className="fa fa-comment-o">
															<span>{blog.comment_count}</span>
														</a>
													</li>
												</ul>
											</div>
											<div className="card-data">
												<div className="card-content">
													<div className="cardtag">
														{blog.tags &&
														blog.tags.map((tag, index) => (
															<span key={index} className="label">
                                    {tag}
                                  </span>
														))}
													</div>
													<span className="author">{blog.author}</span>
													<h4 className="title">{blog.title}</h4>
													<div className="text">
														<ReactSafeHtml html={blog.content}/>
													</div>
													<Link
														to={`${this.props.match.url}/${blog.slug}`}
														onClick={() => this._blogModalToggle(index)}
														className="button button-single-post"
													>
														Read more
													</Link>
												</div>
											</div>
										</div>
									</div>
								</div>
							))
							: <Loader/>}
						{/* <div className="paginationcontainer">
                            <div className="pagination p1">
                                <div className="pagination-container">
                                    <a href="" className="paginnav">
                                        <span>
                                            PREVIOUS</span>
                                    </a>
                                    <a href="">
                                        <span>1</span>
                                    </a>
                                    <a href="" className="is-active">
                                        <span>2</span>
                                    </a>
                                    <a href="">
                                        <span>3</span>
                                    </a>
                                    <a href="">
                                        <span>...</span>
                                    </a>
                                    <a href="">
                                        <span>28</span>
                                    </a>
                                    <a href="" className="paginnav">
                                        <span>NEXT</span>
                                    </a>
                                </div>
                            </div>
                        </div> */}
					</div>
				</div>
			</PageScroll>
		);
	}
}

export default withRouter(Blog);

import React, { Component } from 'react'
import './services.css'
import PageScroll from '../../../utils/PageScroll'
import WOW from 'wow.js/dist/wow.js'
import 'animate.css'
import frontend from './frontend.png'
import mobile from './mobile.png'
import backend from './backend.png'
import responsive from './responsive.png'
import code from './code.png'
import animation from './animation.png'


class Services extends Component {
    state = {
        sectionClass: 'fadeIn',
    };

    componentDidMount() {
        this._wowAnimation()
        this.setState({
            sectionClass: 'fadeIn'
        })
    }

    _wowAnimation = () => {
        if (this.ref) {
            new WOW().init();
        }
    }

    render() {
        return (
            <PageScroll className={`blog page-current page wow ${this.state.sectionClass} animated`}>
                <div className="featured-background">
                    <div className="backgroundimg">
                        <div className="headerdes-container">
                            <fieldset>
                                <legend>WHAT I CAN DO FOR YOU</legend>
                                <i className="peicon pe-7s-tools" aria-hidden="true"></i>
                                <h3>MY SERVICES</h3>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div className="thecontainer">
                    <div className="row row-wrapper">
                        <div className="col-md-12">
                            <div className="title-container">
                                <div className="titlebg">
                                    <div className="icon-title"><i className="fa fa-television" aria-hidden="true"></i></div>
                                    <div className="smalltitle">Services</div>
                                </div>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-browser"></i> */}
                                <div className="service-img">
                                    <img className="img-fluid" src={frontend} alt="" />
                                </div>
                                <h5>FrontEnd</h5>
                                <p>HTML, CSS, & JavaScript - there’s web standards and then there’s browsers, and I know both.
                                </p>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-phone"></i>0 */}
                                <div className="service-img">
                                    <img className="img-fluid" src={mobile} alt="" />
                                </div>
                                <h5>Mobile Development</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscingVivam ipsum dolor sit amet, consectetur
                                    adipiscingVivam
                                </p>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-graph1"></i> */}
                                <div className="service-img">
                                    <img className="img-fluid" src={backend} alt="" />
                                </div>
                                <h5>Backend</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscingVivam
                                    ipsum dolor sit amet, consectetur adipiscingVivam
                                </p>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-graph1"></i> */}
                                <div className="service-img">
                                    <img className="img-fluid" src={responsive} alt="" />
                                </div>
                                <h5>RESPONSIVE DESIGN</h5>
                                <p>No need to worry about what your website looks across
                                    devices, I specialize in bringing full desktop sites
                                    down to the smallest mobile, and in between.
                                    Consistency from your PSDs to beautiful usable mobile
                                    layouts is key.
                                </p>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-diskette"></i> */}
                                <div className="service-img">
                                    <img className="img-fluid" src={code} alt="" />
                                </div>
                                <h5>ACCESSIBILITY & SEMANTICS</h5>
                                <p>Semantics and accessibility are important to code standards.
                                    Descriptive but short classes and commented code allow for easy
                                    revisions later on. Proper use of alt tags and labels ensure users
                                    with assistive devices can easily use your website, too.
                                </p>
                            </div>
                        </div>
                        <div className="servicecol col-md-4 col-sm-4">
                            <div className="service-iconbox">
                                {/* <i className="peicon pe-7s-diskette"></i> */}
                                <div className="service-img">
                                    <img className="img-fluid" src={animation} alt="" />
                                </div>
                                <h5>ANIMATION</h5>
                                <p> enjoy using a mix of CSS animation,
                                    jQuery and SVG animation to bring websites to life.
                                    I usually end up combining all three for interesting effects.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </PageScroll>
        );
    }
}

export default Services;

import React, { Component } from 'react'
import { Switch, Route, withRouter} from "react-router-dom";
import './main-section.css'
import Home from './Home'
import About from './About'
import Portfolio from './Portfolio'
import Contact from './Contact'
import Blog from './Blog'
import Services from './Services'
import { NoMatch } from "../../utils/404";


class MainSection extends Component {

  state = {
    _left: true,
    _right: true,
  }

  _rightSidebar = () => {
    this.setState({_right: !this.state._right}, 
      ()=>this.props._rightSidebar(this.state._right))
  }

  _leftSidebar = () => {
    this.setState({_left: !this.state._left}, 
      ()=>this.props._leftSidebar(this.state._left))
  }

  render() {
    const url = this.props.match.url 
    return (
      <div className="main-section">
        <a onClick={this._leftSidebar} className="btn btn-link" id="left-menu-toggle">
          <span className="peicon pe-7s-menu"></span>
        </a>
        <a onClick={this._rightSidebar} className="btn btn-link " id="right-menu-toggle">
          <span className="peicon pe-7s-user"></span>
        </a>
        <Switch>
          <Route path={`${url}/`} component={Home} />
          <Route path={`${url}about`} component={About} />
          <Route path={`${url}services`} component={Services} />
          <Route path={`${url}portfolio`} component={Portfolio} />
          <Route path={`${url}contact`} component={Contact} />
          <Route path={`${url}blog`} component={Blog} />
          <Route render={props=> (
            <NoMatch {...props} history={this.props.history}/>
          )} />
        </Switch>
      </div>
    );
  }
}
export default withRouter(MainSection);
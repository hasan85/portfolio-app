import React, { Component } from 'react'
import firebase from 'firebase'
import './portfolio.css'
import PageScroll from '../../../utils/PageScroll'
import WOW from 'wow.js/dist/wow.js'
import 'animate.css'
import { LoaderDots as Loader } from '../../../utils/Loaders'

class Portfolio extends Component {

    state = {
        loading: true,
        sectionClass: 'fadeIn',
        portfolioObjects: [],
    };

    _getCards = () => {
        this.setState({ loading: true });
        firebase.database().ref('portfolio').orderByKey().on('value', ss => {
            const data = ss.val();
            const _data = [];
            if (data) {
                Object.keys(data).forEach(key => {
                    _data.push(data[key]);
                });
                _data.reverse();
                this.setState({
                    portfolioObjects: _data,
                    loading: false,
                });
            } else {
                this.setState({
                    portfolioObjects: _data,
                    loading: false,
                });
            }
        });
    };

    componentDidMount() {
        this._getCards()
        this._wowAnimation()
        this.setState({
            sectionClass: 'fadeIn'
        })
    }

    componentWillUnmount() {
        this.setState({
            sectionClass: 'slideOutUp'
        })
    }

    _wowAnimation = () => {
        if (this.ref) {
            new WOW().init();
        }
    }

    render() {
        const { loading, portfolioObjects } = this.state
        return (
            <PageScroll className={`portfolio page-current page wow ${this.state.sectionClass} animated`}>
                <div className="featured-background">
                    <div className="backgroundimg">
                        <div className="headerdes-container">
                            <fieldset>
                                <legend>WHAT I HAVE DO SO FAR</legend>
                                <i className="peicon pe-7s-portfolio" aria-hidden="true"></i>
                                <h3>PORTFOLIO</h3>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div className="thecontainer">
                    <div className="row row-wrapper">
                        <div className="col-md-4 col-sm-12 portfcountcol">
                            <div className="portfocount_container ">
                                <span>Projects number</span> <span className="pnumbercount">{portfolioObjects.length}</span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {portfolioObjects && !loading
                            ? portfolioObjects.map((project, index) => (
                                <div key={index} className="col-xs-12 col-sm-6 col-md-4 portitem wow animated bounceInUp">
                                    <div className="portfolioitem-container">
                                        <div className="portfolioitem-layer"></div> 
                                        <figure className="portfolio-item">
                                            <img className="img-fluid" src={project.image.url} alt="" />
                                        </figure>
                                        <div className="portfolio-cardcontent">
                                            <a rel="noopener noreferrer" target="_blank" className="magnifictrig openBtn" href={project.link}><i className="pe-7s-link"></i></a>
                                            <span className="portfoiocard-title">{project.title}</span>
                                            <p>{project.description}</p>
                                        </div>
                                        <div className="portfolio-cardfooter">
                                            <span className="portfolio-label">{project.selectedType}</span>
                                            <span className="portfolio-date">{project.date}</span>
                                        </div>
                                    </div>
                                </div>
                            ))
                            : <Loader />
                        }
                    </div>
                </div>
            </PageScroll>
        );
    }
}

export default Portfolio;

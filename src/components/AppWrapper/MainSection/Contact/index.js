import React, { Component } from 'react'
import firebase from "firebase"
import './contact.css'
import PageScroll from '../../../utils/PageScroll'
import _ from "lodash"
import WOW from 'wow.js/dist/wow.js'
import 'animate.css'
import { ToastContainer, toast } from 'react-toastify'
import { css } from 'glamor';

const initialState = {
    name: "",
    email: "",
    message: "",
    date: "",
    isLoading: false,
}

class Contact extends Component {

    state = {
        sectionClass: 'fadeIn',
        errors: {},
        ...initialState
    };

    componentDidMount() {
        this._wowAnimation()
        this.setState({
            sectionClass: 'fadeIn'
        })
    }

    _wowAnimation = () => {
        if (this.ref) {
            new WOW().init();
        }
    }

    handleChange = e => this.setState({
        isLoading: false,
        [e.target.name]: e.target.value
    })

    isValid = () => {
        const { name, email, message } = this.state
        const errors = {}
        if (!name) {
            errors.name = "This field is required"
        }
        if (!email) {
            errors.email = "This field is required"
        }
        if (!message) {
            errors.message = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => this.setState({ ...initialState })

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { name, email, message } = this.state
        if (this.isValid()) {
            const self = this
            const messageObj = { name, email, message, date: Date() }
            firebase.database().ref("messages").push(messageObj)
                .then(ref => {
                    self.notify()
                    self.resetState()
                    self.setState({ isLoading: false })
                })
        } else {
            console.log("is not valid")
        }
    }

    notify = () => {
        toast("Your message has been successfully sent..", {
            position: toast.POSITION.BOTTOM_RIGHT,
            className: css({
                background: "#263238",
                color:"#04A8F4"
            })
        });
    };

    render() {
        const { isLoading, errors, message, name, email } = this.state
        return (
            <PageScroll className={`contact page-current page wow ${this.state.sectionClass} animated`}>
                <div className="featured-background">
                    <div className="backgroundimg">
                        <div className="headerdes-container">
                            <fieldset>
                                <legend>CONTACT ME</legend>
                                <i className="peicon pe-7s-mail-open-file" aria-hidden="true"></i>
                                <h3>CONTACT</h3>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div className="thecontainer">
                    <div className="row">
                        <div className="col-md-7">
                            <div className="title-container">
                                <div className="titlebg">
                                    <div className="icon-title"> <i className="fa fa-heart"></i></div>
                                    <div className="smalltitle">Drop Us A Line</div>
                                </div>
                            </div>
                            {/* Contact Form */}
                            <fieldset id="contact-form">
                                <form>
                                    <div id="result"></div>
                                    <input
                                        name="name"
                                        placeholder="NAME"
                                        type="text"
                                        value={name}
                                        onChange={this.handleChange}
                                    />
                                    <b>{errors.name}</b>
                                    <input
                                        name="email"
                                        placeholder="EMAIL"
                                        type="email"
                                        value={email}
                                        onChange={this.handleChange}
                                    />
                                    <b>{errors.email}</b>
                                    <textarea
                                        name="message"
                                        placeholder="MESSAGE"
                                        value={message}
                                        onChange={this.handleChange}
                                    >
                                    </textarea>
                                    <div><b>{errors.message}</b></div>
                                    <button disabled={isLoading} onClick={this.handleSubmit} className="btn-default btn btn-sm">SEND MESSAGE</button>
                                </form>
                            </fieldset>
                            {/* End Contact Form */}
                        </div>

                        <div className="col-md-5 backgroundmap">
                            <div className="title-container">
                                <div className="titlebg">
                                    <div className="icon-title"> <i className="fa fa-heart"></i></div>
                                    <div className="smalltitle">Let's Get In touch</div>
                                </div>
                            </div>
                            <div className="contactinfo">
                                <i className="peicon pe-7s-home" aria-hidden="true"></i>
                                <div className="contactinfo-detail">
                                    <h4>ADRESS</h4>
                                    <span>Nizami street 203B, AF Business House</span>
                                </div>
                            </div>
                            <div className="contactinfo">
                                <i className="peicon pe-7s-phone" aria-hidden="true"></i>
                                <div className="contactinfo-detail">
                                    <h4>PHONE</h4>
                                    <span>+99450 786-48-38</span>
                                </div>
                            </div>
                            <div className="contactinfo">
                                <i className="peicon pe-7s-mail" aria-hidden="true"></i>
                                <div className="contactinfo-detail">
                                    <h4>MAIL</h4>
                                    <span>hasan.tagiyeff@gmail.com</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </PageScroll>
        );
    }
}

export default Contact;

import React, {Component} from 'react';
import firebase from 'firebase';
import './about.css';
import PageScroll from '../../../utils/PageScroll';
// import portrait1 from './portrait1.jpg'
import WOW from 'wow.js/dist/wow.js';
import 'animate.css';
import {LoaderDots as Loader} from '../../../utils/Loaders';

class About extends Component {
  state = {
    loading: true,
    sectionClass: 'fadeIn',
    resumeObjects: [],
    skillObjects: [],
  };

  componentDidMount() {
    this._wowAnimation();
    this.setState({
      sectionClass: 'fadeIn',
    });
    this._getResume();
    this._getSkill();
  }

  _wowAnimation = () => {
    if (this.ref) {
      new WOW().init();
    }
  };

  _getResume = () => {
    this.setState({loading: true});
    firebase
      .database()
      .ref('resumes')
      .orderByKey()
      .on('value', ss => {
        const data = ss.val();
        const _data = [];
        if (data) {
          Object.keys(data).forEach(key => {
            _data.push(data[key]);
          });
          _data.reverse();
          this.setState({
            resumeObjects: _data,
            loading: false,
          });
        } else {
          this.setState({
            resumeObjects: _data,
            loading: false,
          });
        }
      });
  };

  _getSkill = () => {
    this.setState({loading: true});
    firebase
      .database()
      .ref('skills')
      .orderByKey()
      .on('value', ss => {
        const data = ss.val();
        const _data = [];
        if (data) {
          Object.keys(data).forEach(key => {
            _data.push(data[key]);
          });
          _data.reverse();
          this.setState({
            skillObjects: _data,
            loading: false,
          });
        } else {
          this.setState({
            skillObjects: _data,
            loading: false,
          });
        }
      });
  };

  render() {
    const {loading, resumeObjects, skillObjects} = this.state;
    return (
      <PageScroll
        className={`about page-current page wow ${
          this.state.sectionClass
        } animated`}
      >
        <div className="featured-background">
          <div className="backgroundimg">
            <div className="headerdes-container">
              <fieldset>
                <legend>STUDIES AND WORKS</legend>
                <i className="peicon pe-7s-id" aria-hidden="true" />
                <h3>ABOUT ME</h3>
              </fieldset>
            </div>
          </div>
        </div>
        <div className="thecontainer">
          <div className="row row-wrapper">
            <div className="col-md-12">
              <h1 className="Profilename">
                I'm <strong>Hasan Tagiyev</strong>{' '}
              </h1>
              <h5 className="profilejob">Backend Developer</h5>
              <hr />
            </div>
            <div className="aboutcol col-md-6">
              <ul className="personalinfo">
                <li>
                  <span className="personalinfo-tag">Address</span>
                  <span className="personalinfo-item">Azerbaijan, Baku</span>
                </li>
                <li>
                  <span className="personalinfo-tag">E-mail</span>
                  <span className="personalinfo-item">
                    hasantagiyeff@gmail.com
                  </span>
                </li>
                <li>
                  <span className="personalinfo-tag">Phone</span>
                  <span className="personalinfo-item">+ 99470 855 87 10</span>
                </li>
                <li>
                  <span className="personalinfo-tag">Freelance</span>
                  <span className="personalinfo-item av-freelance">
                    Available For Freelance
                  </span>
                </li>
                {/* <li><span className="personalinfo-tag">availability</span><span className="personalinfo-item"><i>On Vacation</i> ti ll may 25, 2018</span></li> */}
              </ul>
            </div>
            <div className="aboutcol col-md-6">
              <p>
                Currently full-time Back-end Developer at Labrin. I love
                building onepage web applications with modern Javascript
                frameworks.
              </p>
              <p>
                I wrote bunch of projects in ReactJs, Python in Django and also
                Ruby on Rails, ASP.NET, Unity. But my favor is React.
              </p>
              <p>
                My goal is to always build applications that are scalable and
                efficient under the hood while providing engaging, pixel-perfect
                user experiences. I am always hungry for new challenges and
                innovations. Check my GITHUB repo.
              </p>
            </div>
          </div>

          <div className="row row-wrapper">
            <div className="col-md-12">
              <hr />
            </div>
          </div>

          <div className="row row-wrapper">
            <div className="aboutcol col-md-6 col-sm-12">
              <div className="title-container">
                <div className="titlebg">
                  <div className="icon-title">
                    <i className="fa fa-user" aria-hidden="true" />
                  </div>
                  <div className="smalltitle">Resume</div>
                </div>
              </div>
              <div className="resume-container">
                <ul className="timeline">
                  <li className="time-label">
                    <span className="bg-light-blue">EDUCATION</span>
                  </li>
                  {resumeObjects && !loading ? (
                    resumeObjects.map(
                      (resume, index) =>
                        resume.selectedType === 'Education' ? (
                          <li key={index}>
                            <i className="peicon pe-7s-study" />
                            <div className="res-sec-date">
                              <span className="firstdate">
                                {resume.year_till}
                              </span>
                              <span>{resume.year_untill}</span>
                            </div>
                            <div className="timeline-item">
                              <span className="time">
                                <i className="pe-7s-map-marker" />
                                <span className="res-location">
                                  {resume.location}
                                </span>
                              </span>
                              <h3 className="timeline-header">
                                {resume.uni_company}
                              </h3>
                              <div className="timeline-body">
                                {resume.description}
                              </div>
                            </div>
                          </li>
                        ) : (
                          ''
                        )
                    )
                  ) : (
                    <div className="loader">
                      <Loader />
                    </div>
                  )}

                  <li className="time-label">
                    <span className="bg-green">JOBS AND EXPERIENCE</span>
                  </li>
                  {resumeObjects && !loading ? (
                    resumeObjects.map(
                      (resume, index) =>
                        resume.selectedType === 'Jobs' ? (
                          <li key={index}>
                            <i className="peicon pe-7s-study" />
                            <div className="res-sec-date">
                              <span className="firstdate">
                                {resume.year_till}
                              </span>
                              <span>{resume.year_untill}</span>
                            </div>
                            <div className="timeline-item">
                              <span className="time">
                                <i className="pe-7s-map-marker" />
                                <span className="res-location">
                                  {resume.location}
                                </span>
                              </span>
                              <h3 className="timeline-header">
                                {resume.uni_company}
                              </h3>
                              <div className="timeline-body">
                                {resume.description}
                              </div>
                            </div>
                          </li>
                        ) : (
                          ''
                        )
                    )
                  ) : (
                    <div className="loader">
                      <Loader />
                    </div>
                  )}
                  <li>
                    <a className="downloadcv" href="">
                      <i className="peicon pe-7s-cloud-download" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            {/* Skills section */}
            <div className="aboutcol col-md-6 col-sm-12">
              <div className="title-container">
                <div className="titlebg">
                  <div className="icon-title">
                    <i className="fa fa-diamond" aria-hidden="true" />
                  </div>
                  <div className="smalltitle">Skills</div>
                </div>
              </div>
              {/* Languages */}
              <div className="skills-container">
                <figure className="skills-item">
                  <i className="peicon pe-7s-monitor" aria-hidden="true" />
                  <span className="skills-title">LANGUAGES</span>
                </figure>
                {skillObjects && !loading ? (
                  skillObjects.map(
                    (skill, index) =>
                      skill.selectedType === 'Language' ? (
                        <div key={index} className="skills-cardfooters">
                          <div className="skills-cardfooter">
                            <span className="skills-label">{skill.name}</span>
                            <div className="bar-container">
                              <div
                                className="bar"
                                style={{width: `${skill.level}%`}}
                              />
                            </div>
                          </div>
                        </div>
                      ) : (
                        ''
                      )
                  )
                ) : (
                  <div className="loader">
                    <Loader />
                  </div>
                )}
              </div>
              {/* END Languages */}
              {/* Frameworks */}
              <div className="skills-container">
                <figure className="skills-item">
                  <i className="peicon pe-7s-monitor" aria-hidden="true" />
                  <span className="skills-title">Frameworks</span>
                </figure>
                {skillObjects && !loading ? (
                  skillObjects.map(
                    (skill, index) =>
                      skill.selectedType === 'Framework' ? (
                        <div key={index} className="skills-cardfooters">
                          <div className="skills-cardfooter">
                            <span className="skills-label">{skill.name}</span>
                            <div className="bar-container">
                              <div
                                className="bar"
                                style={{width: `${skill.level}%`}}
                              />
                            </div>
                          </div>
                        </div>
                      ) : (
                        ''
                      )
                  )
                ) : (
                  <div className="loader">
                    <Loader />
                  </div>
                )}
              </div>
              {/* END Frameworks */}
              {/* Tools */}
              <div className="skills-container">
                <figure className="skills-item">
                  <i className="peicon pe-7s-monitor" aria-hidden="true" />
                  <span className="skills-title">Tools</span>
                </figure>
                {skillObjects && !loading ? (
                  skillObjects.map(
                    (skill, index) =>
                      skill.selectedType === 'Tool' ? (
                        <div key={index} className="skills-cardfooters">
                          <div className="skills-cardfooter">
                            <span className="skills-label">{skill.name}</span>
                            <div className="bar-container">
                              <div
                                className="bar"
                                style={{width: `${skill.level}%`}}
                              />
                            </div>
                          </div>
                        </div>
                      ) : (
                        ''
                      )
                  )
                ) : (
                  <div className="loader">
                    <Loader />
                  </div>
                )}
              </div>
              {/* END Tools */}
              {/* Design */}
              <div className="skills-container">
                <figure className="skills-item">
                  <i className="peicon pe-7s-monitor" aria-hidden="true" />
                  <span className="skills-title">Design</span>
                </figure>
                {skillObjects && !loading ? (
                  skillObjects.map(
                    (skill, index) =>
                      skill.selectedType === 'Design' ? (
                        <div key={index} className="skills-cardfooters">
                          <div className="skills-cardfooter">
                            <span className="skills-label">{skill.name}</span>
                            <div className="bar-container">
                              <div
                                className="bar"
                                style={{width: `${skill.level}%`}}
                              />
                            </div>
                          </div>
                        </div>
                      ) : (
                        ''
                      )
                  )
                ) : (
                  <div className="loader">
                    <Loader />
                  </div>
                )}
              </div>
              {/* END Design */}
            </div>
            {/* END Skills section */}
          </div>
        </div>
      </PageScroll>
    );
  }
}

export default About;

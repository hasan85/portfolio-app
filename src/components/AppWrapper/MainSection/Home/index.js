import React, { Component } from 'react'
import './home.css'
import FullPage from '../../../utils/FullPage'
import WOW from 'wow.js/dist/wow.js'
import 'animate.css'

class Home extends Component {

  constructor(){
    super()
    this.state = {
      
        }
  } 

  componentDidMount() {
    this._wowAnimation()
  }

  _wowAnimation = () => {
    if (this.ref) {
      new WOW().init();
    }
  }

  render() {
    return (
      <div className={`wow fadeIn animated home page page-current`}>
        <div className="mouse-down">
          <div className="bullet"></div>
        </div>
        <FullPage>
          <div className="section slide-one slide-table">
            <h1>HELLO , MY NAME IS</h1>
            <h2>Hasan Tagiyev</h2>
          </div>
          <div className="section slide-two slide-table">
            <h1>I build</h1>
            <h2>interactive websites</h2>
          </div>
          <div className="section slide-three slide-table">
            <h1>that run</h1>
            <h2>across platforms & devices</h2>
          </div>
        </FullPage>
      </div>
    );
  }
}

export default Home;

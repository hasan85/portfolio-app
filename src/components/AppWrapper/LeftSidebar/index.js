import React, { Component } from 'react'
import './left-sidebar.css'
import ScrollBar from '../../utils/ScrollBar'
import { Link, withRouter } from 'react-router-dom'

const menuItems = [
  {
    title: 'Home',
    link: '/',
    icon: 'pe-7s-home'
  },
  {
    title: 'About',
    link: '/about',
    icon: 'pe-7s-id'
  },
  {
    title: 'Services',
    link: '/services',
    icon: 'pe-7s-tools'
  },
  {
    title: 'Portfolio',
    link: '/portfolio',
    icon: 'pe-7s-note2'
  },
  {
    title: 'Blog',
    link: '/blog',
    icon: 'pe-7s-news-paper'
  },
  {
    title: 'Contact',
    link: '/contact',
    icon: 'pe-7s-mail'
  },
]

class LeftSidebar extends Component {
  
  state = {
    activePage: 0,
  }

  componentWillMount(){
    let activePage
    menuItems.forEach((item,i)=>{
      if(item.link === this.props.location.pathname)
        activePage = i
    })
    this.setState({activePage})
  }


  render() {
    const activeClass = "active-page"
    return (
      <ScrollBar className="left-sidebar">
        <nav style={{ marginTop: 10 }}>
          <ul className="main-nav">
            {menuItems.map((item, i) => (
              <li key={i.toString()} onClick={() => this.setState({ activePage: i })}>
                <Link className="pt-trigger" to={item.link}>
                  <i className={`peicon ${item.icon} ${this.state.activePage === i ? activeClass : ""}`} aria-hidden="true"></i>
                  <span >{item.title}</span>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </ScrollBar>
    );
  }
}

export default withRouter(LeftSidebar);
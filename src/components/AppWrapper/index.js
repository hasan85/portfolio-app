import React, { Component } from 'react';
import LeftSidebar from './LeftSidebar'
import RightSidebar from './RightSidebar'
import MainSection from './MainSection'
import { withRouter } from 'react-router-dom';

class AppWrapper extends Component {

    state = {
        openLeftSidebar: true,
        openRightSidebar: true,
    }

    _leftSidebar = open => {
        this.setState({
            openLeftSidebar: open
        })
    }

    _rightSidebar = open => {
        this.setState({
            openRightSidebar: open
        })
    }

    render() {
        const { openLeftSidebar, openRightSidebar } = this.state
        return (
            <div className={`App ${openLeftSidebar ? '' : 'left-toggled'} ${openRightSidebar ? '' : 'right-toggled'}`}>
                <LeftSidebar />
                <RightSidebar />
                <MainSection _leftSidebar={this._leftSidebar} _rightSidebar={this._rightSidebar} />
            </div>
        );
    }
}

export default withRouter(AppWrapper);


import React, { Component } from 'react'
import firebase from 'firebase'
import './right-sidebar.css'
import ScrollBar from '../../utils/ScrollBar'
import { LoaderDots as Loader } from '../../utils/Loaders'

class RightSidebar extends Component {

    state = {
        loading: true,
        activityObjects: [],
    }

    _getCards = () => {
        this.setState({ loading: true });
        firebase.database().ref('activity').orderByKey().on('value', ss => {
            const data = ss.val();
            const _data = [];
            if (data) {
                Object.keys(data).forEach(key => {
                    _data.push(data[key]);
                });
                _data.reverse();
                this.setState({
                    activityObjects: _data,
                    loading: false,
                });
            } else {
                this.setState({
                    activityObjects: _data,
                    loading: false,
                });
            }
        });
    };

    componentDidMount() {
        this._getCards();
    }

    render() {
        const { loading, activityObjects, } = this.state
        return (
            <ScrollBar className="right-sidebar wow slideInLeft">
                <div className="widgets-container">
                    <div className="sidebarwidgets">
                        <div className="img-profile-container">
                            <div className="bio-details">
                                <span className="bio-name"> <strong>Hasan</strong> Tagiyev</span>
                                <span className="bio-more">Backend Developer</span>
                                <span>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.facebook.com/0x00020000"><i className="fa fa-facebook"></i></a>
                                    <a rel="noopener noreferrer" target="_blank" href="https://www.instagram.com/hesen.sm"><i className="fa fa-instagram"></i></a>
                                    <a rel="noopener noreferrer" target="_blank" href="https://github.com/mahwd"><i className="fa fa-github"></i></a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="recent-activity">
                        <ul className="activity-title-ul">
                            <li className="activity-title-li">
                                <span className=" eyebrow-first activity-title">RECENT ACTIVITY</span>
                            </li>
                        </ul>
                        <ul>
                            {activityObjects && !loading
                                ? activityObjects.map((activity, index) => (
                                    <li key={index}>
                                        <span className="eyebrow">{activity.date}</span>
                                        <h1>{activity.title}</h1>
                                        <p>{activity.description}</p>
                                    </li>
                                ))
                                : <Loader />}
                        </ul>
                    </div>
                </div>
            </ScrollBar>
        );
    }
}

export default RightSidebar;
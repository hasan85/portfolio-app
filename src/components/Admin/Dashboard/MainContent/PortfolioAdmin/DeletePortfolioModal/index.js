import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import './delete-modal.css'
import firebase from "firebase";

class DeletePortfolioModal extends Component {

    state = {
        isLoading: false,
        modalStatus:{ isDelete: false, id: "", image_name:""},
    }

    componentWillReceiveProps(nextProps) {
        this.setState({modalStatus: nextProps.modalStatus})
    }

    handleSubmit = e => {
        e.preventDefault()
        const { modalStatus } = this.state
        const self = this
        firebase.database().ref(`portfolio/${modalStatus.id}`).remove()
        .then(ref => {
            firebase.storage().ref(`${modalStatus.image_name}`).delete()
            self.handleClose()
            self.setState({ isLoading: false })
        })
    }

    handleClose = () => {
        this.props.close()
    }

    render() {        
        const { isLoading } = this.state
        return (
            <Modal className="delete-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>
                        Delete Portfolio Card
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    Are you sure?
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={this.handleClose}>Close</Button>
                    <Button disabled={isLoading} onClick={this.handleSubmit} bsStyle="danger">OK</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default DeletePortfolioModal
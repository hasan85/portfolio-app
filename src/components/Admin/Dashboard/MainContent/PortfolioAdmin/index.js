import React, { Component } from 'react';
import firebase from 'firebase'
import { withRouter } from 'react-router-dom';
import PageScroll from '../../../../utils/PageScroll'
import './portfolio-admin.css'
import AddPortfolioModal from './AddPortfolioModal'
import EditPortfolioModal from './EditPortfolioModal'
import DeletePortfolioModal from './DeletePortfolioModal'
import { LoaderDots as Loader } from '../../../../utils/Loaders'


class PortfolioAdmin extends Component {

    state = {
        loading: true,
        modalAddStatus: false,
        modalEditStatus: false,
        modalDeleteStatus: false,
        portfolioObjects: [],
        editProject: null,
        deleteProject: { isDelete: false, id: "", image_name: "" }
    }

    _portfolioAddModalToggle = () => {
        this.setState({
            modalAddStatus: !this.state.modalAddStatus,
        });
    };

    _portfolioEditModalToggle = () => {
        this.setState({
            modalEditStatus: !this.state.modalEditStatus,
        });
    };

    _portfolioDeleteModalToggle = () => this.setState({
        modalDeleteStatus: !this.state.modalDeleteStatus
    })

    _getCards = () => {
        this.setState({ loading: true });
        firebase.database().ref('portfolio').orderByKey().on('value', ss => {
            const data = ss.val();
            const _data = [];
            if (data) {
                Object.keys(data).forEach(key => {
                    _data.push(data[key]);
                });
                _data.reverse();
                this.setState({
                    portfolioObjects: _data,
                    loading: false,
                });
            } else {
                this.setState({
                    portfolioObjects: _data,
                    loading: false,
                });
            }
        });
    };

    componentDidMount() {
        this._getCards();
    }

    handleEdit = index => {
        this.setState(
            { editProject: this.state.portfolioObjects[index] },
            () => this._portfolioEditModalToggle()
        );
    };

    handleDelete = index => {
        const deleteProject = this.state.portfolioObjects[index]
        const { isDelete } = this.state.deleteProject
        this.setState({
            deleteProject: {
                isDelete: !isDelete,
                id: deleteProject.id,
                image_name: deleteProject.image.name
            }
        }, () => this._portfolioDeleteModalToggle()
        );
    }

    render() {
        const { loading, modalAddStatus, portfolioObjects, modalEditStatus, modalDeleteStatus, editProject, deleteProject } = this.state
        return (
            <PageScroll className="portfolio-admin admin-wrapper">
                <div className="admin-container">
                    <AddPortfolioModal open={modalAddStatus} close={this._portfolioAddModalToggle} />
                    <EditPortfolioModal data={editProject} open={modalEditStatus} close={this._portfolioEditModalToggle} />
                    <DeletePortfolioModal open={modalDeleteStatus} modalStatus={deleteProject} close={this._portfolioDeleteModalToggle} />
                    <div className="portfolio-admin-title">
                        <h3>Portfolio</h3>
                        <div onClick={this._portfolioAddModalToggle} className="add-btn">+</div>
                    </div>
                    <div className="row">
                        {portfolioObjects && !loading
                            ? portfolioObjects.map((project, index) => (
                                <div key={index} className="col-xs-12 col-sm-6 col-md-4 portitem">
                                    <div className="portfolioitem-container">
                                        <figure className="portfolio-item">
                                            <img className="img-fluid" src={project.image.url} alt="" />
                                            <div className="portfolioitem-layer"></div>
                                        </figure>
                                        <div className="portfolio-cardcontent">
                                            <span className="portfoiocard-title">{project.title}</span>
                                            <div className="card-btns">
                                                <button
                                                    type="button"
                                                    onClick={() => this.handleEdit(index)}
                                                >
                                                    <i className="fa fa-pencil"></i>
                                                </button>
                                                <button
                                                    type="button"
                                                    onClick={() => this.handleDelete(index)}
                                                >
                                                    <i className="fa fa-trash"></i>
                                                </button>
                                            </div>
                                            <p>{project.description}</p>
                                            <div className="link">{project.link}</div>
                                        </div>
                                        <div className="portfolio-cardfooter">
                                            <span className="portfolio-label">{project.selectedType}</span>
                                            <span className="portfolio-date">{project.date}</span>
                                        </div>
                                    </div>
                                </div>
                            ))
                            : <Loader />}
                    </div>
                </div>
            </PageScroll>
        );
    }
}

export default withRouter(PortfolioAdmin);

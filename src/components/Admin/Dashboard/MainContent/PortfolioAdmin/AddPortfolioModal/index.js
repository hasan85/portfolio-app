import React, { Component } from 'react'
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import './portfolio-modal.css'
import _ from "lodash"
import firebase from "firebase"
import { RippleLoader } from "../../../../../utils/Loaders";


const types = ["Frontend", "Backend", "Full"]

const initialState = {
    title: "",
    link: "",
    date: "",
    description: "",
    selectedType: "Frontend",
    image: null,
    errors: {},
    isLoading: false,
    uploadStatus: false,
}

class AddPortfolioModal extends Component {

    state = {
        ...initialState
    }

    handleChange = e => this.setState({ 
        [e.target.name]: e.target.value 
    })

    handleUpload = e => {
        const image = e.target.files[0]
        this.setState({ image })
    }

    isValid = () => {
        const { title, link, date, description, image } = this.state
        const errors = {}
        if (!title) {
            errors.title = "This field is required"
        }
        if (!link) {
            errors.link = "This field is required"
        }
        if (!date) {
            errors.date = "This field is required"
        }
        if (!description) {
            errors.description = "This field is required"
        }
        if (!image) {
            errors.image = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => this.setState({ ...initialState })

    uploadStorage = image => {
        const imageName = "/portfolio/"+image.name+"&"+image.lastModified
        const storageRef = firebase.storage().ref(imageName)
        const task = storageRef.put(image)
        task.on("state_changed",
            ss => {
                let progress = (ss.bytesTransferred / ss.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
            },
            ex => {
                console.log(ex.message)
            },
            () => {
                console.log("success ^_^ ")
                var downloadURL = task.snapshot.downloadURL;                
                const _image = {}
                _image.name = imageName 
                _image.url = downloadURL
                this.setState({
                    uploadStatus: false,
                    image: _image
                }, () => this.writeDatabase())
            }
        )
    }

    writeDatabase = () => {
        const {uploadStatus, image, link, description, date, selectedType, title} = this.state
        const self = this
        if (!uploadStatus) {
            const portfolioObj = {
                image, 
                link, 
                description, 
                date, 
                selectedType, 
                title
            }
            firebase.database().ref(`portfolio/`).push(portfolioObj)
                .then(ref => {
                    self.handleClose()
                    self.resetState()
                    self.setState({ isLoading: false })
                }).catch(ex=>console.log(ex.message))
        } else {
            console.log("wait for upload complete ...")
        }  
    }

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { image } = this.state
        if (this.isValid()) {
            this.setState({uploadStatus:true},()=>this.uploadStorage(image))
        } else {
            this.setState({
                isLoading: false,
                uploadStatus: false,
            })
        }
    }

    handleClose = () => {
        this.props.close()
        this.resetState()
    }

    handleSelect = e => {
        let { selectedType } = this.state
        selectedType = this.selectedType.value
        this.setState({ selectedType })
    }

    render() {
        const {uploadStatus, isLoading, errors, description, selectedType, title, date, link } = this.state
        return (
            <Modal className="portfolio-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>Add Portfolio Item</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    { isLoading
                        ? <RippleLoader/>
                        : <form>
                            <FormGroup controlId="formControlsTitle">
                                <ControlLabel>Title</ControlLabel>
                                <FormControl
                                    type="text"
                                    name="title"
                                    placeholder="www.ismayilrahimli.com"
                                    value={title}
                                    onChange={this.handleChange}
                                />
                                <HelpBlock>{errors.title}</HelpBlock>
                            </FormGroup>
                            <FormGroup controlId="formControlsType">
                                <ControlLabel>Select Work Type</ControlLabel>
                                <FormControl
                                    onChange={this.handleSelect}
                                    inputRef={el => this.selectedType = el}
                                    defaultValue={selectedType}
                                    componentClass="select" placeholder="select">
                                    {
                                        types.map((_type, index) => (
                                            <option key={index} value={_type}>{_type}</option>
                                        ))
                                    }
                                </FormControl>
                            </FormGroup>
                            <FormGroup controlId="formControlsLink">
                                <ControlLabel>Link</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={link}
                                    placeholder="https://www.facebook.com/"
                                    name="link"
                                    onChange={this.handleChange}
                                />
                                <HelpBlock>{errors.link}</HelpBlock>
                            </FormGroup>
                            <FormGroup controlId="formControlsDate">
                                <ControlLabel>Date</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={date}
                                    placeholder="31/07/2018"
                                    name="date"
                                    onChange={this.handleChange}
                                />
                                <HelpBlock>{errors.date}</HelpBlock>
                            </FormGroup>
                            <FormGroup controlId="formControlsDescription">
                                <ControlLabel>Description</ControlLabel>
                                <FormControl
                                    componentClass="textarea"
                                    placeholder="HTML / CSS / Javascript..."
                                    value={description}
                                    onChange={this.handleChange}
                                    name="description"
                                />
                                <HelpBlock>{errors.description}</HelpBlock>
                            </FormGroup>
                            <input onChange={this.handleUpload} type="file" name="image" />
                            <HelpBlock>{errors.image}</HelpBlock>
                        </form>
                    }
                </Modal.Body>

                <Modal.Footer>
                    <Button disabled={uploadStatus}  onClick={this.handleClose}>Close</Button>
                    <Button disabled={uploadStatus} onClick={this.handleSubmit} bsStyle="primary">Add</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default AddPortfolioModal

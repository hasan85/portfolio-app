import React, { Component } from 'react';
import firebase from 'firebase'
import { withRouter } from 'react-router-dom';
import PageScroll from '../../../../utils/PageScroll'
import './message-admin.css'
import DeleteModal from './DeleteModal'
import { LoaderDots as Loader } from '../../../../utils/Loaders'

class MessageAdmin extends Component {

    state = {
        loading: true,
        deleteModalShow: false,
        modalDeleteStatus: { isDelete: false, id: '' },
        messageObjects: [],
    }

    _deleteModalToggle = () => {
        this.setState({
            deleteModalShow: !this.state.deleteModalShow,
        });
    };

    _getCards = () => {
        this.setState({ loading: true });
        firebase.database().ref('messages').orderByKey().on('value', ss => {
            const data = ss.val();
            const _data = [];
            if (data) {
                Object.keys(data).forEach(key => {
                    _data.push(data[key]);
                });
                _data.reverse();
                this.setState({
                    messageObjects: _data,
                    loading: false,
                });
            } else {
                this.setState({
                    messageObjects: _data,
                    loading: false,
                });
            }
        });
    };

    handleDelete = index => {
        this.setState(
            {
                modalDeleteStatus: {
                    isDelete: true,
                    id: this.state.messageObjects[index].id,
                },
            },
            () => this._deleteModalToggle()
        );
    };

    componentDidMount() {
        this._getCards();
    }

    render() {
        const { loading, messageObjects, modalDeleteStatus, } = this.state
        return (
            <PageScroll className="message-admin admin-wrapper">
                <div className="admin-container">
                    <DeleteModal open={this.state.deleteModalShow} modalDeleteStatus={modalDeleteStatus} closeDelete={this._deleteModalToggle} />
                    <div className="activity-admin-title">
                        <h3>Messages</h3>
                    </div>
                    <div className="row">
                        {messageObjects && !loading
                            ? messageObjects.map((message, index) => (
                                <div key={index} className="col-sm-4 col-md-3">
                                    <div className="message-card">
                                        <div className="card-head">
                                            <h4>{message.name}</h4>
                                            <div className="card-btns">
                                                <button type="button" onClick={() => this.handleDelete(index)}>
                                                    <i className="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <ul>
                                                <li>
                                                    <span>Email: </span>
                                                    <span>{message.email}</span>
                                                </li>
                                                <li>
                                                    <span>Date: </span>
                                                    <span>{message.date.split(' ').slice(0, 4).join(' ')}</span>
                                                </li>
                                                <li>
                                                    <span>Message: </span>
                                                    <span>{message.message}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ))
                            : <Loader />}
                    </div>
                </div>
            </PageScroll >
        );
    }
}

export default withRouter(MessageAdmin);
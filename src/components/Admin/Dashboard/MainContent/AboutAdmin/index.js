import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import firebase from 'firebase';
import PageScroll from '../../../../utils/PageScroll';
import {withRouter} from 'react-router-dom';
import './about-admin.css';
import ResumeModal from './ResumeModal';
import DeleteModal from './DeleteModal';
import SkillModal from './SkillModal';
import SkillDeleteModal from './SkillDeleteModal';
import {LoaderDots as Loader} from '../../../../utils/Loaders';

class AboutAdmin extends Component {
  state = {
    loading: true,
    resumeModalShow: false,
    deleteModalShow: false,
    skillModalShow: false,
    deleteModalShowSkill: false,
    modalEditStatus: {isEdit: false, data: {}},
    skillModalEditStatus: {isEdit: false, data: {}},
    modalDeleteStatus: {isDelete: false, id: ''},
    skillModalDeleteStatus: {isDelete: false, id: ''},
    resumeObjects: [],
    skillObjects: [],
  };

  _resumeModalToggle = () => {
    this.setState({
      resumeModalShow: !this.state.resumeModalShow,
    });
  };

  _skillModalToggle = () => {
    this.setState({
      skillModalShow: !this.state.skillModalShow,
    });
  };

  _deleteModalToggle = () => {
    this.setState({
      deleteModalShow: !this.state.deleteModalShow,
    });
  };

  _deleteModalToggleSkill = () => {
    this.setState({
      deleteModalShowSkill: !this.state.deleteModalShowSkill,
    });
  };

  _getCards = () => {
    this.setState({loading: true});
    firebase
      .database()
      .ref('resumes')
      .orderByKey()
      .on('value', ss => {
        const data = ss.val();
        const _data = [];
        if (data) {
          Object.keys(data).forEach(key => {
            _data.push(data[key]);
          });
          _data.reverse();
          this.setState({
            resumeObjects: _data,
            loading: false,
          });
        } else {
          this.setState({
            resumeObjects: _data,
            loading: false,
          });
        }
      });
  };

  _getSkillCards = () => {
    this.setState({loading: true});
    firebase
      .database()
      .ref('skills')
      .orderByKey()
      .on('value', ss => {
        const data = ss.val();
        const _data = [];
        if (data) {
          Object.keys(data).forEach(key => {
            _data.push(data[key]);
          });
          _data.reverse();
          this.setState({
            skillObjects: _data,
            loading: false,
          });
        } else {
          this.setState({
            skillObjects: _data,
            loading: false,
          });
        }
      });
  };

  componentDidMount() {
    this._getCards();
    this._getSkillCards();
  }

  handleDelete = index => {
    this.setState(
      {
        modalDeleteStatus: {
          isDelete: true,
          id: this.state.resumeObjects[index].id,
        },
      },
      () => this._deleteModalToggle()
    );
  };

  handleDeleteSkill = index => {
    this.setState(
      {
        skillModalDeleteStatus: {
          isDelete: true,
          id: this.state.skillObjects[index].id,
        },
      },
      () => this._deleteModalToggleSkill()
    );
  };

  handleEdit = index => {
    this.setState(
      {
        modalEditStatus: {
          isEdit: true,
          data: this.state.resumeObjects[index],
        },
      },
      () => this._resumeModalToggle()
    );
  };

  handleEditSkill = index => {
    this.setState(
      {
        skillModalEditStatus: {
          isEdit: true,
          data: this.state.skillObjects[index],
        },
      },
      () => this._skillModalToggle()
    );
  };

  render() {
    const {
      loading,
      resumeObjects,
      modalEditStatus,
      skillModalEditStatus,
      modalDeleteStatus,
      skillModalDeleteStatus,
      skillObjects,
    } = this.state;
    return (
      <PageScroll className="about-admin admin-wrapper">
        <div className="admin-container">
          <ResumeModal
            cardUpdate={this._getCards}
            options={modalEditStatus}
            open={this.state.resumeModalShow}
            close={this._resumeModalToggle}
          />
          <DeleteModal
            open={this.state.deleteModalShow}
            modalDeleteStatus={modalDeleteStatus}
            closeDelete={this._deleteModalToggle}
          />
          <SkillModal
            skillCardUpdate={this._getSkillCards}
            options={skillModalEditStatus}
            open={this.state.skillModalShow}
            close={this._skillModalToggle}
          />
          <SkillDeleteModal
            open={this.state.deleteModalShowSkill}
            modalDeleteStatus={skillModalDeleteStatus}
            closeDelete={this._deleteModalToggleSkill}
          />
          <div className="row">
            <div className="col-sm-6 col-md-6">
              <div className="about-admin-title">
                <h3>Resume</h3>
                <div onClick={this._resumeModalToggle} className="add-btn">
                  +
                </div>
              </div>
              {resumeObjects && !loading ? (
                resumeObjects.map((resume, index) => (
                  <div className="resume-card" key={index}>
                    <div className="card-head">
                      <h4>{resume.uni_company}</h4>
                      <div className="card-btns">
                        <button
                          type="button"
                          onClick={() => this.handleEdit(index)}
                        >
                          <i className="fa fa-pencil" />
                        </button>
                        <button
                          type="button"
                          onClick={() => this.handleDelete(index)}
                        >
                          <i className="fa fa-trash" />
                        </button>
                      </div>
                    </div>
                    <div className="card-body">
                      <ul>
                        <li>
                          <span>Type: </span>
                          <span>{resume.selectedType}</span>
                        </li>
                        <li>
                          <span>Years: </span>
                          <span>{resume.year_till}</span> -{' '}
                          <span>{resume.year_untill}</span>
                        </li>
                        <li>
                          <span>Location: </span>
                          <span>{resume.location}</span>
                        </li>
                        <li>
                          <span>Description: </span>
                          <span>{resume.description}</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                ))
              ) : (
                <div className="loader">
                  <Loader />
                </div>
              )}
            </div>
            <div className="col-sm-6 col-md-6">
              <div className="about-admin-title">
                <h3>Skills</h3>
                <div onClick={this._skillModalToggle} className="add-btn">
                  +
                </div>
              </div>
              {skillObjects && !loading ? (
                <Table striped bordered condensed hover>
                  <tbody>
                    {skillObjects.map((skill, index) => (
                      <tr key={index}>
                        <td>{skill.name}</td>
                        <td>{skill.selectedType}</td>
                        <td>{`${skill.level} %`}</td>
                        <td>
                          <span className="card-btns">
                            <button
                              type="button"
                              onClick={() => this.handleEditSkill(index)}
                            >
                              <i className="fa fa-pencil" />
                            </button>
                            <button
                              type="button"
                              onClick={() => this.handleDeleteSkill(index)}
                            >
                              <i className="fa fa-trash" />
                            </button>
                          </span>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              ) : (
                <Loader />
              )}
            </div>
          </div>
        </div>
      </PageScroll>
    );
  }
}

export default withRouter(AboutAdmin);

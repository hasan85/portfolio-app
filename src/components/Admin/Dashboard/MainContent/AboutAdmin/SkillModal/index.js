import React, { Component } from 'react'
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import './skill-modal.css'
import _ from "lodash";
import firebase from "firebase";

const types = ["Language", "Framework", "Tool", "Design"]

const initialState = {
    name: "",
    level: "",
    selectedType: "Language",
    isLoading: false,
}

class SkillModal extends Component {

    state = {
        errors: {},
        options: {
            ...initialState
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.options.isEdit) {
            this.setState({ options: this.props.options.data })
        }
    }

    handleChange = e => {
        const { options } = this.state
        options[e.target.name] = e.target.value
        this.setState({ options })
    }

    isValid = () => {
        const { options } = this.state
        const errors = {}
        if (!options.name) {
            errors.name = "This field is required"
        }
        if (!options.level) {
            errors.level = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => this.setState({
        options: {
            ...initialState
        }
    })

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { options } = this.state
        if (this.isValid()) {
            const self = this
            if (this.props.options.isEdit) {
                firebase.database().ref(`skills/${options.id}`).set(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            } else {
                firebase.database().ref("skills").push(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            }
        } else {
            console.log("is not valid")
        }
    }

    handleClose = () => {
        this.props.skillCardUpdate()
        this.props.options.isEdit = false
        this.props.close()
        this.resetState()
    }

    handleSelect = e => {
        const { options } = this.state
        options.selectedType = this.selectedType.value
        this.setState({ options })
    }

    render() {
        const {errors} = this.state
        const { isLoading, name, selectedType, level,} = this.state.options
        return (
            <Modal className="skill-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>{this.props.options.isEdit ? 'Edit Skill Card' : 'Add Skill Card'}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <form >
                        <FormGroup controlId="formControlsType">
                            <ControlLabel>Select Skill Type</ControlLabel>
                            <FormControl
                                onChange={this.handleSelect}
                                inputRef={el => this.selectedType = el}
                                defaultValue={selectedType}
                                componentClass="select" placeholder="select">
                                {
                                    types.map((_type, index) => (
                                        <option key={index} value={_type}>{_type}</option>
                                    ))
                                }
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="formControlsName">
                            <ControlLabel>Skill Name</ControlLabel>
                            <FormControl
                                type="text"
                                name="name"
                                value={name}
                                placeholder="React"
                                onChange={this.handleChange}
                            />
                            <HelpBlock>{errors.name}</HelpBlock>
                        </FormGroup>

                        <FormGroup controlId="formControlsLevel">
                            <ControlLabel>Skill Level (0 - 100)</ControlLabel>
                            <FormControl
                                type="number"
                                placeholder="70"
                                value={level}
                                name="level"
                                onChange={this.handleChange}
                            />
                            <HelpBlock>{errors.level}</HelpBlock>
                        </FormGroup>
                    </form>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={this.handleClose}>Close</Button>
                    <Button disabled={isLoading} onClick={this.handleSubmit} bsStyle="primary">{this.props.options.isEdit ? 'Edit' : 'Add'}</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default SkillModal

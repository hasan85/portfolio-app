import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import './skill-delete.css'
// import _ from "lodash";
import firebase from "firebase";

class SkillDeleteModal extends Component {

    state = {
        isLoading: false,
        modalStatus:{ isDelete: false, id: ""},
    }

    componentDidMount() {
        this.setState({modalStatus: this.props.modalDeleteStatus})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({modalStatus: nextProps.modalDeleteStatus})
    }

    handleSubmit = e => {
        e.preventDefault()
        const { modalStatus } = this.state        
        const self = this
        firebase.database().ref(`skills/${modalStatus.id}`).remove()
        .then(ref => {
            self.handleClose()
            self.setState({ isLoading: false })
        })
    }

    handleClose = () => {
        this.props.closeDelete()
    }

    render() {        
        const { isLoading } = this.state
        return (
            <Modal className="delete-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>
                        Delete Resume Card
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    Are you sure?
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={this.handleClose}>Close</Button>
                    <Button disabled={isLoading} onClick={this.handleSubmit} bsStyle="danger">OK</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default SkillDeleteModal
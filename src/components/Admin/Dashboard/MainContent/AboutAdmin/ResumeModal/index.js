import React, { Component } from 'react'
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import './resume-modal.css'
import _ from "lodash";
import firebase from "firebase";

const types = ["Education", "Jobs"]

const initialState = {
    uni_company: "",
    year_till: "",
    year_untill: "",
    description: "",
    location: "",
    selectedType: "Education",
    isLoading: false,
}

class ResumeModal extends Component {

    state = {
        errors: {},
        options: {
            ...initialState
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.options.isEdit) {
            this.setState({ options: this.props.options.data })
        }
    }

    handleChange = e => {
        const { options } = this.state
        options[e.target.name] = e.target.value
        this.setState({ options })
    }


    isValid = () => {
        const { options } = this.state
        const errors = {}
        if (!options.uni_company) {
            errors.uni_company = "This field is required"
        }
        if (!options.year_till) {
            errors.year_till = "This field is required"
        }
        if (!options.year_untill) {
            errors.year_untill = "This field is required"
        }
        if (!options.description) {
            errors.description = "This field is required"
        }
        if (!options.location) {
            errors.location = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => this.setState({
        options: {
            ...initialState
        }
    })

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { options } = this.state
        if (this.isValid()) {
            const self = this
            if (this.props.options.isEdit) {
                firebase.database().ref(`resumes/${options.id}`).set(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            } else {
                firebase.database().ref("resumes").push(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            }
        } else {
            console.log("is not valid")
        }
    }

    handleClose = () => {
        this.props.cardUpdate()
        this.props.options.isEdit = false
        this.props.close()
        this.resetState()
    }

    handleSelect = e => {
        const { options } = this.state
        options.selectedType = this.selectedType.value
        this.setState({ options })
    }

    render() {
        const {errors} = this.state
        const { isLoading, description, selectedType, location, uni_company, year_till, year_untill } = this.state.options
        return (
            <Modal className="resume-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>{this.props.options.isEdit ? 'Edit Resume Card' : 'Add Resume Card'}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <form >
                        <FormGroup controlId="formControlsType">
                            <ControlLabel>Select Resume Type</ControlLabel>
                            <FormControl
                                onChange={this.handleSelect}
                                inputRef={el => this.selectedType = el}
                                defaultValue={selectedType}
                                componentClass="select" placeholder="select">
                                {
                                    types.map((_type, index) => (
                                        <option key={index} value={_type}>{_type}</option>
                                    ))
                                }
                            </FormControl>
                        </FormGroup>
                        <FormGroup controlId="formControlsJobEdu">
                            <ControlLabel>Company / Education Center</ControlLabel>
                            <FormControl
                                type="text"
                                name="uni_company"
                                value={uni_company}
                                onChange={this.handleChange}
                                placeholder="National Aviation Academy"
                            />
                            <HelpBlock>{errors.uni_company}</HelpBlock>
                        </FormGroup>
                        <div className="row">
                            <div className="col-md-6">
                                <FormGroup controlId="formControlsFrom">
                                    <ControlLabel>From Year</ControlLabel>
                                    <FormControl
                                        type="text"
                                        name="year_till"
                                        value={year_till}
                                        onChange={this.handleChange}
                                        placeholder="2011"
                                    />
                                    <HelpBlock>{errors.year_till}</HelpBlock>
                                </FormGroup>
                            </div>
                            <div className="col-md-6">
                                <FormGroup controlId="formControlsTo">
                                    <ControlLabel>To Year</ControlLabel>
                                    <FormControl
                                        type="text"
                                        name="year_untill"
                                        value={year_untill}
                                        onChange={this.handleChange}
                                        placeholder="2015"
                                    />
                                    <HelpBlock>{errors.year_untill}</HelpBlock>
                                </FormGroup>
                            </div>
                        </div>
                        <FormGroup controlId="formControlsLocation">
                            <ControlLabel>Location</ControlLabel>
                            <FormControl
                                type="text"
                                value={location}
                                name="location"
                                onChange={this.handleChange}
                                placeholder="Baku"
                            />
                            <HelpBlock>{errors.location}</HelpBlock>                            
                        </FormGroup>
                        <FormGroup controlId="formControlsDescription">
                            <ControlLabel>Description</ControlLabel>
                            <FormControl
                                componentClass="textarea"
                                placeholder="Computer Engineering / Master degree"
                                value={description}
                                onChange={this.handleChange}
                                name="description"
                            />
                            <HelpBlock>{errors.description}</HelpBlock>                                                        
                        </FormGroup>

                    </form>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={this.handleClose}>Close</Button>
                    <Button disabled={isLoading} onClick={this.handleSubmit} bsStyle="primary">{this.props.options.isEdit ? 'Edit' : 'Add'}</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default ResumeModal

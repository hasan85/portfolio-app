import React, {Component} from 'react';
import firebase from 'firebase';
import {withRouter} from 'react-router-dom';
import PageScroll from '../../../../utils/PageScroll';
import './blog-admin.css';
import AddModal from './AddModal';
import EditModal from './EditModal';
import DeleteModal from './DeleteModal';
import CommentModal from './CommentModal';
import ReactSafeHtml from 'react-safe-html';
import {LoaderDots as Loader} from '../../../../utils/Loaders';

class BlogAdmin extends Component {
  state = {
    loading: true,
    addModalShow: false,
    deleteModalShow: false,
    commentModalShow: false,
    commentModalStatus: {},
    modalEditStatus: {isEdit: false, data: {}},
    modalDeleteStatus: false,
    deleteBlog: {isDelete: false, id: '', image_name: ''},
    blogObjects: [],
    comments: [],
  };

  _addModalToggle = () => {
    this.setState ({
      addModalShow: !this.state.addModalShow,
    });
  };

  _commentModalToggle = () => {
    this.setState ({
      commentModalShow: !this.state.commentModalShow,
    });
  };

  _editModalToggle = () => {
    this.setState ({
      modalEditStatus: !this.state.modalEditStatus,
    });
  };

  _deleteModalToggle = () => {
    if (this.state.deleteModalShow) {
      this._getCards()
    }
    this.setState ({
      deleteModalShow: !this.state.deleteModalShow,
    });
  };

  _getComments = () => {
    const {blogObjects} = this.state;
    if (blogObjects) {
      blogObjects.forEach (blog => {
        let comment_count = 0;
        if (blog.id) {
          firebase
            .database ()
            .ref ('comments')
            .orderByChild ('post_id')
            .equalTo (blog.id)
            .on ('value', ss => {
              const comments = ss.val ();
              if (comments) {
                comment_count = Object.keys (comments).length;
              } else {
                comment_count = 0;
              }
              blog.comment_count = comment_count;
              this.setState ({blogObjects});
            });
        }
      });
    }
  };

  _getCards = () => {
    this.setState ({loading: true});
    firebase.database ().ref ('blog').orderByKey ().on ('value', ss => {
      const data = ss.val ();
      const _data = [];
      if (data) {
        Object.keys (data).forEach (key => {
          const blog = data[key];
          _data.push (blog);
        });
        _data.reverse ();
      }
      this.setState (
        {
          blogObjects: _data,
          loading: false,
        },
        () => this._getComments ()
      );
    });
  };

  handleDelete = index => {
    this.handleComment (index, () => {
      console.log('callback');
      const deleteBlog = this.state.blogObjects[index];
      const {isDelete} = this.state.deleteBlog;
      this.setState (
        {
          deleteBlog: {
            isDelete: !isDelete,
            id: deleteBlog.id,
            image_name: deleteBlog.image.name,
          },
        },
        () => this._deleteModalToggle ()
      );
    });
  };

  handleEdit = index => {
    this.setState (
      {
        modalEditStatus: {isEdit: true, data: this.state.blogObjects[index]},
      },
      () => this._editModalToggle ()
    );
  };

  handleComment = (index, callback) => {
    const blog = this.state.blogObjects[index];
    console.log("handleComment");
    firebase
      .database ()
      .ref ('comments')
      .orderByChild ('post_id')
      .equalTo (blog.id)
      .on ('value', ss => {
        const _comments = ss.val ();
        if (_comments) {
          const comments = [];
          console.log('_comments',_comments);
          Object.keys (_comments).forEach (key => {
            comments.push (_comments[key]);
          });
          this.setState ({comments}, () => {
            console.log("comments", comments);
            if (callback) {
              callback ();
            }else {
              this.setState ({commentModalShow: true})
            }
          }
          );
        } else if(!callback){
          alert ('no comments to delete ...');
        } else {
          callback()
        }
      });
  };

  componentDidMount () {
    this._getCards ();
  }

  render () {
    const {
      loading,
      blogObjects,
      deleteBlog,
      modalEditStatus,
      deleteModalShow,
      commentModalShow,
      comments,
    } = this.state;
    return (
      <PageScroll className="blog-admin admin-wrapper">
        <div className="admin-container">
          <AddModal
            cardUpdate={this._getCards}
            open={this.state.addModalShow}
            close={this._addModalToggle}
          />
          <EditModal
            data={modalEditStatus.data}
            open={modalEditStatus.isEdit}
            close={this._editModalToggle}
          />
          <DeleteModal
            open={deleteModalShow}
            modalStatus={deleteBlog}
            comments={comments}
            close={this._deleteModalToggle}
          />
          <CommentModal
            open={commentModalShow}
            comments={comments}
            close={this._commentModalToggle}
          />
          <div className="activity-admin-title">
            <h3>Blog</h3>
            <div onClick={this._addModalToggle} className="add-btn">+</div>
          </div>
          <div className="row">
            {blogObjects && !loading
              ? blogObjects.map ((blog, index) => (
                  <div key={index} className="col-sm-6 col-xs-12 col-md-4">
                    <div className="blog-card">
                      <div
                        className="card-wrapper"
                        style={{
                          background: `url(${blog.image.url}) center/cover no-repeat`,
                        }}
                      >
                        <div className="card-wrapper-layer" />
                        <div className="card-header">
                          <div className="date">
                            <span className="postday">{blog.date}</span>
                          </div>
                          <ul className="menu-content">
                            <li>
                              <div
                                className="fa fa-comment-o"
                                onClick={() => this.handleComment (index)}
                              >
                                <span>{blog.comment_count}</span>
                              </div>
                            </li>
                            <li>
                              <button
                                type="button"
                                onClick={() => this.handleEdit (index)}
                              >
                                <i className="fa fa-pencil" />
                              </button>
                            </li>
                            <li>
                              <button
                                type="button"
                                onClick={() => this.handleDelete (index)}
                              >
                                <i className="fa fa-trash" />
                              </button>
                            </li>
                          </ul>
                        </div>
                        <div className="card-data">
                          <div className="card-content">
                            <div className="cardtag">
                              {blog.tags &&
                                blog.tags.map ((tag, index) => (
                                  <span key={index} className="label">
                                    {tag}
                                  </span>
                                ))}
                            </div>
                            <span className="author">{blog.author}</span>
                            <h4 className="title">{blog.title}</h4>
                            <div className="text">
                              <ReactSafeHtml html={blog.content} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              : <Loader />}
          </div>
        </div>
      </PageScroll>
    );
  }
}

export default withRouter (BlogAdmin);

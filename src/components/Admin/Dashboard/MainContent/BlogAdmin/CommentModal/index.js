import React, {Component} from 'react'
import {Modal, Button} from 'react-bootstrap'
import './comment-modal.css'
import firebase from 'firebase'

class CommentModal extends Component {
  state = {
    isLoading: false,
    comments: [],
    delete_list: [],
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.comments) {
      this.setState ({comments: nextProps.comments})
    }
  }

  handleSubmit = e => {
    e.preventDefault ()
    const {delete_list} = this.state
    delete_list.forEach (id => {
      firebase.database ().ref (`/comments/${id}`).remove ()
    })
    this.props.close()
  }

  handleDelete = id => {
    const {delete_list, comments} = this.state
    let index = 0
    comments.forEach ((item, i) => {
      if (item.id === id) {
        index = i
      }
    })
    console.log("index ->", index)
    comments.splice (index, 1)
    delete_list.push (id)
    this.setState ({
        comments, 
        delete_list
    })
  }

  handleClose = () => {
    this.props.close ()
  }

  render () {
    const {isLoading, comments} = this.state
    return (
      <Modal
        className="comment-modal"
        show={this.props.open}
        onHide={this.handleClose}
      >
        <Modal.Header>
          <Modal.Title>
            Delete Comment
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <ol className="comment-list">
            {comments.map ((comment, i) => (
              <li key={i}>
                <div className="comment">
                  <div
                    onClick={() => this.handleDelete (comment.id)}
                    className="comment-delete"
                  >
                    <i className="fa fa-times-circle" />
                  </div>
                  <div className="comment-header">
                    <p className="comment-date">
                      On {comment.date}
                    </p>
                    <h3 className="comment-title">
                      <strong>{comment.name}</strong> commented:
                    </h3>
                  </div>
                  <div className="comment-body">
                    <p>
                      {comment.comment}
                    </p>
                  </div>
                </div>
              </li>
            ))}
          </ol>
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.handleClose}>Close</Button>
          <Button
            disabled={isLoading}
            onClick={this.handleSubmit}
            bsStyle="danger"
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default CommentModal

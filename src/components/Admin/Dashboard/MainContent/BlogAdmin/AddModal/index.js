import React, { Component } from 'react'
import {
    Modal,
    Button,
    FormGroup,
    ControlLabel,
    FormControl,
    HelpBlock
} from 'react-bootstrap'
import './add-modal.css'
import _ from "lodash"
import firebase from "firebase"
import { RippleLoader } from "../../../../../utils/Loaders"
import CKEditor from "react-ckeditor-component"

const initialState = {
    title: "",
    content: "",
    date: "",
    author: "",
    tags: [],
    newTag: "",
    image: null,
    errors: {},
    isLoading: false,
    uploadStatus: false,
}

class AddModal extends Component {

    state = {
        ...initialState
    }

    // ck editor
    updateContent = newContent => {
        this.setState({
            content: newContent
        })
    }

    onChange = evt => {
        var newContent = evt.editor.getData();
        this.setState({
            content: newContent
        })
    }

    onBlur = evt => {
    }

    afterPaste = evt => {
    }

    //End Ck editor

    handleChange = e => this.setState({
        [e.target.name]: e.target.value
    })

    handleTagsSubmit = e => {
        if (e.keyCode === 13) {
            e.preventDefault()
            const { tags } = this.state
            const { newTag } = this.state
            tags.push(newTag)
            this.setState({
                tags,
                newTag: "",
            })
        }
    }

    _resetTags = () => {
        this.setState({
            tags: []
        })
    }

    deleteTag = tag => {
        const { tags } = this.state
        tags.splice(tags.indexOf(tag), 1)
        this.setState({
            tags
        })
    }

    handleUpload = e => {
        const image = e.target.files[0]
        this.setState({ image })
    }

    isValid = () => {
        const { title, author, date, content, image } = this.state
        const errors = {}
        if (!title) {
            errors.title = "This field is required"
        }
        if (!content) {
            errors.content = "This field is required"
        }
        if (!date) {
            errors.date = "This field is required"
        }
        if (!author) {
            errors.author = "This field is required"
        }
        if (!image) {
            errors.image = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => {
        this.setState({
            ...initialState 
        })
        this._resetTags()    
    }

    uploadStorage = image => {
        const imageName = "/blog/" + image.name + "&" + image.lastModified
        const storageRef = firebase.storage().ref(imageName)
        const task = storageRef.put(image)
        task.on("state_changed",
            ss => {
                let progress = (ss.bytesTransferred / ss.totalBytes) * 100;
                console.log('Upload is ' + progress + '% done');
            },
            ex => {
                console.log(ex.message)
            },
            () => {
                console.log("success ^_^ ")
                var downloadURL = task.snapshot.downloadURL;
                const _image = {}
                _image.name = imageName
                _image.url = downloadURL
                this.setState({
                    uploadStatus: false,
                    image: _image
                }, () => this.writeDatabase())
            }
        )
    }

    writeDatabase = () => {
        const { uploadStatus, image, title, author, date, content, tags } = this.state
        const self = this
        if (!uploadStatus) {
            const slug = title
                .replace(" ", "-")
                .replace("ə", "e")
                .replace("ğ", "g")
                .replace("ş", "sh")
                .replace("ı", "i")
                .replace("ö", "o")
                .replace("ü", "u")
                .replace("ç", "ch")
                .toLowerCase()
            const blogObj = {
                image,
                title,
                content,
                date,
                tags,
                author,
                slug
            }
            firebase.database().ref(`blog/`).push(blogObj)
                .then(ref => {
                    self.handleClose()
                    self.resetState()
                    self.setState({ isLoading: false })
                }).catch(ex => console.log(ex.message))
        } else {
            console.log("wait for upload complete ...")
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { image } = this.state
        if (this.isValid()) {
            this.setState({ uploadStatus: true }, () => this.uploadStorage(image))
        } else {
            this.setState({
                isLoading: false,
                uploadStatus: false,
            })
        }
    }

    handleClose = () => {
        this.props.close()
        this.resetState()
    }

    render() {
        const { uploadStatus, newTag, isLoading, errors, title, author, content, date, tags } = this.state
        return (
            <Modal className="add-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>Add Blog Item</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {isLoading
                        ? <RippleLoader />
                        : <form>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <FormGroup controlId="formControlsTitle">
                                        <ControlLabel>Title</ControlLabel>
                                        <FormControl
                                            type="text"
                                            name="title"
                                            placeholder="Blog Title"
                                            value={title}
                                            onChange={this.handleChange}
                                        />
                                        <HelpBlock>{errors.title}</HelpBlock>
                                    </FormGroup>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <FormGroup controlId="formControlsAuthor">
                                        <ControlLabel>Author</ControlLabel>
                                        <FormControl
                                            type="text"
                                            placeholder="Rahimli Ismayil"
                                            value={author}
                                            onChange={this.handleChange}
                                            name="author"
                                        />
                                        <HelpBlock>{errors.author}</HelpBlock>
                                    </FormGroup>
                                </div>
                            </div>
                            <FormGroup controlId="formControlsContent">
                                <ControlLabel>Content</ControlLabel>
                                <CKEditor
                                    activeClass="p10"
                                    content={content}
                                    events={{
                                        "blur": this.onBlur,
                                        "afterPaste": this.afterPaste,
                                        "change": this.onChange
                                    }}
                                />
                                <HelpBlock>{errors.content}</HelpBlock>
                            </FormGroup>
                            <div className="row">
                                <div className="col-sm-6 col-md-6">
                                    <FormGroup controlId="formControlsDate">
                                        <ControlLabel>Date</ControlLabel>
                                        <FormControl
                                            type="text"
                                            value={date}
                                            placeholder="12 Feb 2018"
                                            name="date"
                                            onChange={this.handleChange}
                                        />
                                        <HelpBlock>{errors.date}</HelpBlock>
                                    </FormGroup>
                                </div>
                                <div className="col-sm-6 col-md-6">
                                    <FormGroup controlId="formControlsImage">
                                        <ControlLabel>Image</ControlLabel>
                                        <input onChange={this.handleUpload} type="file" name="image" />
                                        <HelpBlock>{errors.image}</HelpBlock>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    {tags.map((tag, index) => (
                                        <span key={index} className="tag">
                                            {tag}
                                            <span onClick={() => this.deleteTag(tag)} className="remove-tag">
                                                <i className="fa fa-times-circle"></i>
                                            </span>
                                        </span>
                                    ))}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-5 col-md-4">
                                    <FormGroup controlId="formControlstags" onKeyDown={this.handleTagsSubmit}>
                                        <ControlLabel>Add Tag</ControlLabel>
                                        <FormControl
                                            type="text"
                                            value={newTag}
                                            placeholder="Programming"
                                            name="newTag"
                                            onChange={this.handleChange}
                                        />
                                    </FormGroup>
                                </div>
                            </div>
                        </form>
                    }
                </Modal.Body>

                <Modal.Footer>
                    <Button disabled={uploadStatus} onClick={this.handleClose}>Close</Button>
                    <Button disabled={uploadStatus} onClick={this.handleSubmit} bsStyle="primary">Add</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default AddModal

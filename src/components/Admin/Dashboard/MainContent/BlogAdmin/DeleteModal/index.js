import React, {Component} from 'react'
import {Modal, Button} from 'react-bootstrap'
import './delete-modal.css'
import firebase from 'firebase'
import _ from "lodash"


class DeleteModal extends Component {
  state = {
    isLoading: false,
    modalStatus: {isDelete: false, id: '', image_name: ''},
  }

  componentWillReceiveProps (nextProps) {
    this.setState ({
      modalStatus: nextProps.modalStatus,
      comments: nextProps.comments,
    })
  }

  deleteComments = () => {
    const {comments} = this.state
    if (_.isEmpty (comments)) {
        this.handleClose ()
        this.setState ({isLoading: false})
      } else {
        comments.forEach ((comment, i) => {
          console.log ('comment->', comment)
          firebase
            .database ()
            .ref (`comments/${comment.id}`)
            .remove ()
            .then (() => {
              console.log (`${comment.id} deleted`)
              if (i === comments.length - 1) {
                this.setState ({isLoading: false}, ()=>this.handleClose ())
              }
            })
        })
      }
  }

  handleSubmit = e => {
    e.preventDefault ()
    const {modalStatus} = this.state
    firebase.database ().ref (`blog/${modalStatus.id}`).remove ().then (ref => {
      firebase.storage ().ref (`${modalStatus.image_name}`).delete ()
      this.deleteComments()
    })
  }

  handleClose = () => {
    this.props.close ()
  }

  render () {
    const {isLoading} = this.state
    return (
      <Modal
        className="delete-modal"
        show={this.props.open}
        onHide={this.handleClose}
      >
        <Modal.Header>
          <Modal.Title>
            Delete Blog Card
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          Are you sure?
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.handleClose}>Close</Button>
          <Button
            disabled={isLoading}
            onClick={this.handleSubmit}
            bsStyle="danger"
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default DeleteModal

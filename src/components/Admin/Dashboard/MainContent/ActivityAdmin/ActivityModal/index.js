import React, { Component } from 'react'
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import './activity-modal.css'
import _ from "lodash";
import firebase from "firebase";

const initialState = {
    title: "",
    date: "",
    description: "",
    isLoading: false,
}

class ActivityModal extends Component {

    state = {
        errors: {},
        options: {
            ...initialState
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.options.isEdit) {
            this.setState({ options: this.props.options.data })
        }
    }

    handleChange = e => {
        const { options } = this.state
        options[e.target.name] = e.target.value
        this.setState({ options })
    }

    isValid = () => {
        const { options } = this.state
        const errors = {}
        if (!options.title) {
            errors.title = "This field is required"
        }
        if (!options.date) {
            errors.date = "This field is required"
        }
        if (!options.description) {
            errors.description = "This field is required"
        }
        this.setState({ errors })
        return _.isEmpty(errors)
    }

    resetState = () => this.setState({
        options: {
            ...initialState
        }
    })

    handleSubmit = e => {
        e.preventDefault()
        this.setState({ isLoading: true })
        const { options } = this.state
        if (this.isValid()) {
            const self = this
            if (this.props.options.isEdit) {
                firebase.database().ref(`activity/${options.id}`).set(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            } else {
                firebase.database().ref("activity").push(options)
                    .then(ref => {
                        self.handleClose()
                        self.resetState()
                        self.setState({ isLoading: false })
                    })
            }
        } else {
            console.log("is not valid")
        }
    }

    handleClose = () => {
        this.props.cardUpdate()
        this.props.options.isEdit = false
        this.props.close()
        this.resetState()
    }

    render() {
        const {errors} = this.state
        const { isLoading, description, title, date, } = this.state.options
        return (
            <Modal className="activity-modal" show={this.props.open} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>{this.props.options.isEdit ? 'Edit Activity Card' : 'Add Activity Card'}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <form >
                        <FormGroup controlId="formControlsTitle">
                            <ControlLabel>Activity Title</ControlLabel>
                            <FormControl
                                type="text"
                                name="title"
                                value={title}
                                onChange={this.handleChange}
                                placeholder="Meeting"
                            />
                            <HelpBlock>{errors.title}</HelpBlock>
                        </FormGroup>
                        <FormGroup controlId="formControlsLocation">
                            <ControlLabel>Date</ControlLabel>
                            <FormControl
                                type="text"
                                value={date}
                                name="date"
                                onChange={this.handleChange}
                                placeholder="Jan 15th, 2016"
                            />
                            <HelpBlock>{errors.date}</HelpBlock>                            
                        </FormGroup>
                        <FormGroup controlId="formControlsDescription">
                            <ControlLabel>Description</ControlLabel>
                            <FormControl
                                componentClass="textarea"
                                placeholder="lorem ipsum"
                                value={description}
                                onChange={this.handleChange}
                                name="description"
                            />
                            <HelpBlock>{errors.description}</HelpBlock>                                                        
                        </FormGroup>
                    </form>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={this.handleClose}>Close</Button>
                    <Button disabled={isLoading} onClick={this.handleSubmit} bsStyle="primary">{this.props.options.isEdit ? 'Edit' : 'Add'}</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default ActivityModal

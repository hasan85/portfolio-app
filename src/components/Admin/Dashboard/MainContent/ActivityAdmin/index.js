import React, { Component } from 'react';
import firebase from 'firebase'
import { withRouter } from 'react-router-dom';
import PageScroll from '../../../../utils/PageScroll'
import './activity-admin.css'
import ActivityModal from './ActivityModal'
import DeleteModal from './DeleteModal'
import { LoaderDots as Loader } from '../../../../utils/Loaders'

class ActivityAdmin extends Component {

    state = {
        loading: true,
        activityModalShow: false,
        deleteModalShow: false,
        modalEditStatus: { isEdit: false, data: {} },
        modalDeleteStatus: { isDelete: false, id: '' },
        activityObjects: [],
    }

    _activityModalToggle = () => {
        this.setState({
            activityModalShow: !this.state.activityModalShow,
        });
    };

    _deleteModalToggle = () => {
        this.setState({
            deleteModalShow: !this.state.deleteModalShow,
        });
    };

    _getCards = () => {
        this.setState({ loading: true });
        firebase.database().ref('activity').orderByKey().on('value', ss => {
            const data = ss.val();
            const _data = [];
            if (data) {
                Object.keys(data).forEach(key => {
                    _data.push(data[key]);
                });
                _data.reverse();
                this.setState({
                    activityObjects: _data,
                    loading: false,
                });
            } else {
                this.setState({
                    activityObjects: _data,
                    loading: false,
                });
            }
        });
    };

    handleDelete = index => {
        this.setState(
            {
                modalDeleteStatus: {
                    isDelete: true,
                    id: this.state.activityObjects[index].id,
                },
            },
            () => this._deleteModalToggle()
        );
    };

    handleEdit = index => {
        this.setState(
            {
                modalEditStatus: {
                    isEdit: true,
                    data: this.state.activityObjects[index],
                },
            },
            () => this._activityModalToggle()
        );
    };

    componentDidMount() {
        this._getCards();
    }

    render() {
        const { loading, activityObjects, modalEditStatus, modalDeleteStatus, } = this.state
        return (
            <PageScroll className="activity-admin admin-wrapper">
                <div className="admin-container">
                    <ActivityModal cardUpdate={this._getCards} options={modalEditStatus} open={this.state.activityModalShow} close={this._activityModalToggle} />
                    <DeleteModal open={this.state.deleteModalShow} modalDeleteStatus={modalDeleteStatus} closeDelete={this._deleteModalToggle} />
                    <div className="activity-admin-title">
                        <h3>Recently Activity</h3>
                        <div onClick={this._activityModalToggle} className="add-btn">+</div>
                    </div>
                    <div className="row">
                        {activityObjects && !loading
                            ? activityObjects.map((activity, index) => (
                                <div key={index} className="col-sm-4 col-md-3">
                                    <div className="activity-card">
                                        <div className="card-head">
                                            <h4>{activity.title}</h4>
                                            <div className="card-btns">
                                                <button type="button" onClick={() => this.handleEdit(index)}>
                                                    <i className="fa fa-pencil"></i>
                                                </button>
                                                <button type="button" onClick={() => this.handleDelete(index)}>
                                                    <i className="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <ul>
                                                <li>
                                                    <span>Date: </span>
                                                    <span>{activity.date}</span>
                                                </li>
                                                <li>
                                                    <span>Description: </span>
                                                    <span>{activity.description}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ))
                            : <Loader />}
                    </div>
                </div>
            </PageScroll >
        );
    }
}

export default withRouter(ActivityAdmin);
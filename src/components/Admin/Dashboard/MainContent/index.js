import React, { Component } from 'react';
import { Switch, Route, withRouter } from "react-router-dom";
import AboutAdmin from './AboutAdmin'
import HomeAdmin from './HomeAdmin'
import PortfolioAdmin from './PortfolioAdmin'
import MessageAdmin from './MessageAdmin'
import BlogAdmin from './BlogAdmin'
import ActivityAdmin from './ActivityAdmin'
import './main-content.css'
import { NoMatch } from "../../../utils/404";


class MainContent extends Component {

    render() {
        const url = this.props.match.url
        return (
            <div className="content-wrapper">
                <Switch >
                    <Route exact path={`${url}/`} component={HomeAdmin} />
                    <Route exact path={`${url}/about`} component={AboutAdmin} />
                    <Route exact path={`${url}/portfolio`} component={PortfolioAdmin} />
                    <Route exact path={`${url}/blog`} component={BlogAdmin} />
                    <Route exact path={`${url}/message`} component={MessageAdmin} />
                    <Route exact path={`${url}/activity`} component={ActivityAdmin} />
                    <Route render={props => (
                        <NoMatch {...props} history={this.props.history} />
                    )} />
                </Switch>
            </div>
        );
    }
}

export default withRouter(MainContent);
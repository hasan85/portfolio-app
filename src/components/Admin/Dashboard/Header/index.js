import React, { Component } from 'react'
import './header.css'
import firebase from 'firebase'
import { withRouter  } from 'react-router'
import {Link} from 'react-router-dom'
class Header extends Component {
  state = {
    _left: true,
  }

  _leftSidebar = () => {
    this.setState({ _left: !this.state._left },
      () => this.props._leftSidebar(this.state._left))
  }

  signOut = () => {
    firebase
      .auth()
      .signOut()
      .then(() => this.props.history.push(this.props.match.url))
      .catch(ex => console.log(ex.message));
  };

  render() {
    return (
      <header>
        <Link className="logo" to="/">
          <b>Admin</b>
        </Link>
        <nav className="navbar navbar-static-top">
          <a onClick={this._leftSidebar} className="btn btn-link left-menu-toggle">
            <span className="peicon pe-7s-menu"></span>
          </a>
          <div className="sign-out" onClick={this.signOut}>
            Sign out
          </div>
        </nav>
      </header>
    );
  }
}

export default withRouter(Header);

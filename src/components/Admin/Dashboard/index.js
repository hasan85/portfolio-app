import React, { Component } from 'react';
import Header from './Header'
import LeftSide from './LeftSide'
import MainContent from './MainContent'
import './dashboard.css'
import { withRouter } from 'react-router-dom';

class Dashboard extends Component {

    state = {
        openLeftSidebar: true,
    }

    _leftSidebar = open => {
        this.setState({
            openLeftSidebar: open
        })
    }

    render() {
        const { openLeftSidebar } = this.state        
        return (
            <div className={`dashboard ${openLeftSidebar ? '' : 'left-toggled'}`}>
                <Header _leftSidebar={this._leftSidebar}/>
                <LeftSide />
                <MainContent />
            </div>
        );
    }
}

export default withRouter(Dashboard);



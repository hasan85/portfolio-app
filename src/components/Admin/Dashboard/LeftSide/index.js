import React, { Component } from 'react';
import './left-side.css'
// import Iso from './iso.jpeg'
import { Link, withRouter } from 'react-router-dom'
import ScrollBar from '../../../utils/ScrollBar'

const sidebarMenu = [
    {
        title: 'Dashboard',
        link: '/admin/dashboard',
        icon: 'pe-7s-home'
    },
    {
        title: 'About',
        link: '/admin/dashboard/about',
        icon: 'pe-7s-id'
    },
    {
        title: 'Portfolio',
        link: '/admin/dashboard/portfolio',
        icon: 'pe-7s-note2'
    },
    {
        title: 'Blog',
        link: '/admin/dashboard/blog',
        icon: 'pe-7s-news-paper'
    },
    {
        title: 'Message',
        link: '/admin/dashboard/message',
        icon: 'pe-7s-mail'
    },
    {
        title: 'Activity',
        link: '/admin/dashboard/activity',
        icon: 'pe-7s-wristwatch'
    },
]

class LeftSide extends Component {

    state = {
        activePage: 0,
    }

    componentWillMount() {
        let activePage
        sidebarMenu.forEach((item, i) => {
            if (item.link === this.props.location.pathname)
                activePage = i
        })
        this.setState({ activePage })
    }

    render() {
        const activeClass = "active"
        return (
            <ScrollBar className="left-side">
                <nav style={{ marginTop: 5 }}>
                    <ul className="main-nav">
                        {sidebarMenu.map((item, i) => (
                            <li key={i} onClick={() => this.setState({ activePage: i })}>
                                <Link className={`pt-trigger ${this.state.activePage === i ? activeClass : ""}`} to={item.link}>
                                    <i className={`peicon ${item.icon}`} aria-hidden="true"></i>
                                    <span >{item.title}</span>
                                </Link>
                            </li>
                        ))}
                    </ul>
                </nav>
            </ScrollBar>
        );
    }
}

export default withRouter(LeftSide);
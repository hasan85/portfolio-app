import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'
import { HelpBlock } from 'react-bootstrap'
import { emailPattern } from '../../utils/pattern'
import firebase from 'firebase'
import { registerUser } from '../../../redux/actions'
import './login.css'
import { connect } from "react-redux"

class Login extends Component {
  state = {
    email: '',
    password: '',
    errors: {},
    emailError: '',
    passwordError: '',
  };

  handleSubmit = e => {
    // eslint-disable-next-line
    e ? e.preventDefault() : undefined;
    const { email, password, emailError, passwordError } = this.state;
    if (
      (email && password && _.isEmpty(emailError), _.isEmpty(passwordError))
    ) {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          this.props.dispatch(registerUser(user));
          //   this.props.history.push (`${this.props.match.url}/dashboard`);
        })
        .catch(ex => console.log(ex.message));
    }
  };

  checkCreditials = (email, pass, activeInput) => {
    if (activeInput === 'email') this.checkEmail(email);
    else this.checkPass(pass);
  };

  checkEmail = email => {
    if (emailPattern.test(email)) this.setState({ emailError: '' });
    else this.setState({ emailError: 'Email is not valid valid' });
  };

  checkPass = pass => {
    if (pass.length < 6)
      this.setState({ passwordError: 'Password is too short' });
    else this.setState({ passwordError: '' });
  };

  onChange = e => {
    e.persist();
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        this.checkCreditials(
          this.state.email,
          this.state.password,
          e.target.name
        );
      }
    );
  };

  render() {
    const { emailError, passwordError, email, password } = this.state;
    return (
      <div id="login-form">
        <i className="peicon pe-7s-user" aria-hidden="true" />
        <h3>Only Heso</h3>
        <form onSubmit={this.handleSubmit}>
          <input
            id="email"
            onChange={this.onChange}
            name="email"
            value={email}
            placeholder="EMAIL"
            type="email"
          />
          <HelpBlock>
            {!_.isEmpty(emailError) ? emailError : ''}
          </HelpBlock>
          <input
            id="password"
            onChange={this.onChange}
            name="password"
            value={password}
            placeholder="PASSWORD"
            type="password"
          />
          <HelpBlock>
            {!_.isEmpty(passwordError) ? passwordError : ''}
          </HelpBlock>
          <button
            type="submit"
            className="btn-default btn btn-sm"
            id="login_btn"
          >
            LOGIN
          </button>
          {/* <div id="result"></div> */}
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default withRouter(connect(mapStateToProps)(Login))

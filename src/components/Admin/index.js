import React, {Component} from 'react';
import {withRouter, Switch, Route} from 'react-router-dom';
import Dashboard from './Dashboard';
import Login from './Login';
import {NoMatch} from '../../components/utils/404';
import firebase from 'firebase';
import {connect} from 'react-redux';
import {LoaderCubeSlider as Loader} from '../utils/Loaders';
import { loaderIgnitor } from '../../redux/actions';



const style = {
  loaderContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}
class Admin extends Component {

  componentDidMount () {
    firebase.auth ().onAuthStateChanged (user => {
      if (user) {
        const {pathname} = this.props.location;
        let {url} = this.props.match;
        if (pathname === '/admin/' || pathname === '/admin') {
          url = url.split ('').reverse ()[0] === '/'
            ? url + 'dashboard'
            : url + '/dashboard';
          this.props.dispatch(loaderIgnitor({isLoading:false}))
          this.props.history.push (url);
        } else {
          this.props.history.push (`${this.props.location.pathname}`);
          this.props.dispatch(loaderIgnitor({isLoading:false}))          
        }
      } else {
        this.props.history.push (`${this.props.match.url}`);
        this.props.dispatch(loaderIgnitor({isLoading:false}))
      }
    });
  }

  state = {};

  render () {
    const {loading} = this.props;
    return loading
      ? <div style={style.loaderContainer}>
        <Loader />
      </div>
      : <Switch>
          <Route exact path={`${this.props.match.url}/`} component={Login} />
          <Route
            path={`${this.props.match.url}/dashboard`}
            component={Dashboard}
          />
          <Route
            render={props => (
              <NoMatch {...props} history={this.props.history} />
            )}
          />
        </Switch>;
  }
}

const mapStateToProps = state => {
  return {
    loading: state.loader.isLoading,
  };
};

export default withRouter (connect (mapStateToProps) (Admin));

import React from 'react';
import ReactDOM from 'react-dom';
//import '../node_modules/bootstrap/dist/css/bootstrap.css';
//import '../node_modules/bootstrap/dist/css/bootstrap-theme.css';
import './App.css';
import App from './App';
import './_assets/css/pe-icon-7-stroke.css';
import '../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css';
import 'font-awesome/css/font-awesome.css';
import {Router} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import registerServiceWorker from './registerServiceWorker';
import {config} from './config';
import firebase from 'firebase';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducers from "./redux/reducers";

export const store = createStore (
  reducers
//   applyMiddleware ( logger )
);

const history = createBrowserHistory ();
firebase.initializeApp (config);

ReactDOM.render (
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById ('root')
);
registerServiceWorker ();

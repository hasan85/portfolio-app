import React, { Component } from 'react';
import AppWrapper from './components/AppWrapper'
import { withRouter, Switch, Route } from "react-router-dom";
import Admin from './components/Admin'
import { NoMatch } from "./components/utils/404";

class App extends Component {

  state = {}

  render() {
    return (
      <Switch>
        <Route path={`${this.props.match.url}admin`} component={Admin} />
        <Route path={`${this.props.match.url}`} component={AppWrapper} />
        <Route render={props=> (
            <NoMatch {...props} history={this.props.history}/>
          )} />
      </Switch>
    );
  }
}

export default withRouter(App)

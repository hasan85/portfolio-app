const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


exports.resumeId = functions.database.ref('/resumes/{pushId}')
.onWrite(event => {
  const resume = event.data.val();
  if (!resume && resume.id) return
  resume.id = event.params.pushId
  return event.data.ref.set(resume)
});

exports.skillId = functions.database.ref('/skills/{pushId}')
.onWrite(event => {
  const skill = event.data.val();
  if (!skill && skill.id) return
  skill.id = event.params.pushId
  return event.data.ref.set(skill)
});

exports.projectId = functions.database.ref('/portfolio/{pushId}')
.onWrite(event => {
  const project = event.data.val();
  if (!project && project.id) return
  project.id = event.params.pushId
  return event.data.ref.set(project)
});

exports.blogId = functions.database.ref('/blog/{pushId}')
.onWrite(event => {
  const blog = event.data.val();
  if (!blog && blog.id) return
  blog.id = event.params.pushId
  return event.data.ref.set(blog)
});

exports.activityId = functions.database.ref('/activity/{pushId}')
.onWrite(event => {
  const activity = event.data.val();
  if (!activity && activity.id) return
  activity.id = event.params.pushId
  return event.data.ref.set(activity)
});

exports.messageId = functions.database.ref('/messages/{pushId}')
.onWrite(event => {
  const message = event.data.val();
  if (!message && message.id) return
  message.id = event.params.pushId
  return event.data.ref.set(message)
});

exports.commentId = functions.database.ref('/comments/{pushId}')
.onWrite(event => {
  const comment = event.data.val();
  if (!comment && comment.id) return
  comment.id = event.params.pushId
  return event.data.ref.set(comment)
});



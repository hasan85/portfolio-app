const express = require('express')
const path = require('path')
const port = process.env.PORT || 7575
const app = express()

app.use(express.static(path.join(__dirname, 'build')))
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'))
})

const server = app.listen(7575, () => {
    console.log(`Server started on ${server.address().address}:${server.address().port}`);
})